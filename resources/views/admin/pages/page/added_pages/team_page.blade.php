
{{--
    add section info
    
    addSection($name, $type, $pages, $value = '', $pos)
    Type EDITOR = 1;
    Type ATTACHMENT = 2;
    Type FORM = 3;
    Type TEXTAREA = 4;

--}}

{{  addSection('Image Section 1',2,$page->id,'public/images/team-overview-banner.jpg',1)  }}
{{  addSection('Content Section 1',1,$page->id,'  <h1>Our Team</h1>
<h2>Experts from the Entire Value-Chain</h2>

<p>Decades of experience in Power Quality, Development, Design/ Engineering, Operations & Maintenance (O&M).</p>',2)  }}


{{  addSection('Section 7 Image',2,$page->id,'public/images/join-team.jpg',37)  }}

{{  addSection('Section 7 Content',1,$page->id,'  <h3>Join Our Team</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>',38)  }}

{{  addSection('Button Text Section 7',3,$page->id,'Contact',39)  }}
{{  addSection('Button Link Section 7',3,$page->id,'contact-us',40)  }}

