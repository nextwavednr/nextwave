
{{--
    add section info
    
    addSection($name, $type, $pages, $value = '', $pos)
    Type EDITOR = 1;
    Type ATTACHMENT = 2;
    Type FORM = 3;
    Type TEXTAREA = 4;

--}}

{{  addSection('Image Section 1',2,$page->id,'public/images/homebanner-img.jpg',1)  }}
{{  addSection('Content Section 1',1,$page->id,'<h1>About Next Wave</h1>
<h2>Informed Decisions & Diagnostics Based on Real Data</h2>

<p>The Solution to Effectively Monitor Your Distributed Energy Resources (DER).</p>',2)  }}

{{-- {{  addSection('Button Text Section 1',3,$page->id,'',3)  }} --}}
{{-- {{  addSection('Button Link Section 1',3,$page->id,'',4)  }} --}}

{{  addSection('Section 2 Header',3,$page->id,'Our mission is to simplify this high volume of data and focus on meaningful analytics to identify root-cause and ultimately, reduce mean time to repair (MTTR).',5)  }}

{{  addSection('Section 2 Header-Image',2,$page->id,'',5.1)  }}
{{  addSection('Section 2 Header-Video-Link',3,$page->id,'',5.2)  }}

{{  addSection('Image Section 2',2,$page->id,'public/images/technologies-bg.png',6)  }}
{{  addSection('Content Section 2',1,$page->id,' <h3>Designed & Built by Industry Experts</h3>
<p>Next Wave’s experience with DER operations and real-world failures has allowed us to create a platform that provides monitoring, improved diagnostics, and fact-based decision making.</p>
<p>Tutorials: remove this option since it will give everyone an in-depth video of our UI and platform capabilities. We will add this to our Customer View UI that you already created.</p>',7)  }}
{{  addSection('Button Text Section 2',3,$page->id,'View Tutorials',8)  }}
{{  addSection('Button Link Section 2',3,$page->id,'#',9)  }}

{{  addSection('Content Section 3',1,$page->id,'<h3>Hands on Services</h3>
<p>Our customer service support understands how renewable energy facilities are intended to perform and the significance of Key Performance Indicators (KPI). This know-how provides an efficient approach to retrofitting existing facilities, new projects and supporting potential complications.</p>',10)  }}
{{  addSection('Button Text Section 3',3,$page->id,'View Our Team',11)  }}
{{  addSection('Button Link Section 3',3,$page->id,'#',12)  }}
{{  addSection('Image Section 3',2,$page->id,'public/images/hands-on-services.jpg',13)  }}


{{  addSection('Section 7 Content',1,$page->id,'<h3>Get In Touch</h3>
<p>Don’t hesitate to contact us to hear more about our performance monitoring and analytics software. We look forward to it. </p>',38)  }}

{{  addSection('Button Text Section 7.1',3,$page->id,'Contact',39)  }}
{{  addSection('Button Link Section 7.1',3,$page->id,'contact-us',40)  }}

{{  addSection('Button Text Section 7.2',3,$page->id,'(858) 224-7360',41)  }}
{{  addSection('Button Link Section 7.2',3,$page->id,'#',42)  }}
