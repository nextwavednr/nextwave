
{{--
    add section info
    
    addSection($name, $type, $pages, $value = '', $pos)
    Type EDITOR = 1;
    Type ATTACHMENT = 2;
    Type FORM = 3;
    Type TEXTAREA = 4;

--}}

{{-- {{  addSection('Service 1 title',3,1,'',1)  }}
{{  addSection('Service 1 description',3,1,'',2)  }}

{{  addSection('Service 2 title',3,1,'',3)  }}
{{  addSection('Service 2 description',3,1,'',4)  }}

{{  addSection('Service 3 title',3,1,'',5)  }}
{{  addSection('Service 3 description',3,1,'',6)  }}

{{  addSection('Service 4 title',3,1,'',7)  }}
{{  addSection('Service 4 description',3,1,'',8)  }}

{{  addSection('Service 5 title',3,1,'',9)  }}
{{  addSection('Service 5 description',3,1,'',10)  }}

{{  addSection('Service 6 title',3,1,'',11)  }}
{{  addSection('Service 6 description',3,1,'',12)  }} --}}


{{-- {{  addSection('Service 1 title',3,$page->id,'',1)  }} --}}


{{  addSection('Image Section 1',2,$page->id,'public/images/homebanner-img.jpg',1)  }}
{{  addSection('Content Section 1',1,$page->id,'  <h1>Renewable Energy Performance Monitoring</h1>
<h2>The Operating System for the Grid of the Future</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>',2)  }}
{{  addSection('Button Text Section 1',3,$page->id,'Our Approach',3)  }}
{{  addSection('Button Link Section 1',3,$page->id,'about-us',4)  }}


{{  addSection('Image Section 2',2,$page->id,'public/images/energy-vitals.png',5)  }}
{{  addSection('Content Section 2',1,$page->id,'<h3>We Deliver Substantive Energy Vitals</h3>
<p>Next Wave Energy Monitoring is a renewable energy performance monitoring & data analytics platform designed by power quality engineers to deliver substantive energy vitals to solar and energy storage system end-users</p>',6)  }}
{{  addSection('Button Text Section 2',3,$page->id,'Learn More',7)  }}
{{  addSection('Button Link Section 2',3,$page->id,'about-us',8)  }}


{{  addSection('Content Section 3',1,$page->id,'  <h3>Developed By Power Quality Engineers</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
',9)  }}
{{  addSection('Button Text Section 3',3,$page->id,'Our Team',10)  }}
{{  addSection('Button Link Section 3',3,$page->id,'team',11)  }}
{{  addSection('Image Section 3',2,$page->id,'public/images/quality-eng.png',12)  }}


{{  addSection('Content Section 4',1,$page->id,'<h3>What We Offer</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>',13)  }}

{{-- {{  addSection('Section 4-1 Name',3,$page->id,'Improved Customer Experience',13.01)  }}
{{  addSection('Section 4-1 Image',2,$page->id,'public/img/01.png',13.02)  }}
{{  addSection('Section 4-1 Text',4,$page->id,'Customer service is a major part of our business ethos.',13.03)  }}

{{  addSection('Section 4-2 Name',3,$page->id,'Analytics',13.04)  }}
{{  addSection('Section 4-2 Image',2,$page->id,'public/img/02.jpg',13.05)  }}
{{  addSection('Section 4-2 Text',4,$page->id,'Anomaly detection, performance trending, plant or fleet-level interface/ data, troubleshooting capabilities and facility loads.',13.06)  }}

{{  addSection('Section 4-3 Name',3,$page->id,'Data Science',13.07)  }}
{{  addSection('Section 4-3 Image',2,$page->id,'public/img/01-2.png',13.08)  }}
{{  addSection('Section 4-3 Text',4,$page->id,'Our platform aggregates data from the plant’s interactive equipment and translates it into meaningful power and energy vitals.',13.09)  }}

{{  addSection('Section 4-4 Name',3,$page->id,'Intuitive UI & Cloud Analytics',13.10)  }}
{{  addSection('Section 4-4 Image',2,$page->id,'public/img/02-2.jpg',13.11)  }}
{{  addSection('Section 4-4 Text',4,$page->id,'Next Wave’s platform focuses on actionable data to address critical issues before they balloon into downtime. Our Cloud-based software has been designed with distinct views for respective customer classes.',13.12)  }}

{{  addSection('Section 4-5 Name',3,$page->id,'Benchmarking & Reporting',13.13)  }}
{{  addSection('Section 4-5 Image',2,$page->id,'public/img/01-3.png',13.14)  }}
{{  addSection('Section 4-5 Text',4,$page->id,'Nex Wave’s real-time data displays performance benchmarks, customizable reporting, alarm prioritization and summaries, irradiance measurements, data exportation, data archiving, carbon equivalences, etc.',13.15)  }} --}}


{{  addSection('Content Section 5',1,$page->id,'<h3>Why Choose Us</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>',14)  }}


{{  addSection('Section 5 Col 1 Name',3,$page->id,'The Hub for Business Processes',15)  }}
{{  addSection('Section 5 Col 1 Icon',2,$page->id,'public/images/wcu-icon01.png',16)  }}
{{  addSection('Section 5 Col 1 Text',4,$page->id,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor',17)  }}

{{  addSection('Section 5 Col 2 Name',3,$page->id,'Data Curation',18)  }}
{{  addSection('Section 5 Col 2 Icon',2,$page->id,'public/images/wcu-icon02.png',19)  }}
{{  addSection('Section 5 Col 2 Text',4,$page->id,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor',20)  }}

{{  addSection('Section 5 Col 3 Name',3,$page->id,'Portfolio Aggregation',21)  }}
{{  addSection('Section 5 Col 3 Icon',2,$page->id,'public/images/wcu-icon03.png',22)  }}
{{  addSection('Section 5 Col 3 Text',4,$page->id,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor',23)  }}

{{  addSection('Section 5 Col 4 Name',3,$page->id,'The Advantage Number Four',24)  }}
{{  addSection('Section 5 Col 4 Icon',2,$page->id,'public/images/wcu-icon04.png',25)  }}
{{  addSection('Section 5 Col 4 Text',4,$page->id,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor',26)  }}


{{-- {{  addSection('Header Text Section 6',3,$page->id,'Stay Informed With Our latest updates',27)  }}

{{  addSection('Section 6 Col 1 Image',2,$page->id,'public/images/news01.jpg',28)  }}
{{  addSection('Section 6 Col 1 Header Text',3,$page->id,'Ut enim ad minum veniam',29)  }}
{{  addSection('Section 6 Col 1 Header Overview Content',4,$page->id,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...',30)  }}
{{  addSection('Section 6 Col 1 Link',3,$page->id,'#',31)  }}

{{  addSection('Section 6 Col 2 Image',2,$page->id,'public/images/news02.jpg',32)  }}
{{  addSection('Section 6 Col 2 Header Text',3,$page->id,'Ut enim ad minum veniam',33)  }}
{{  addSection('Section 6 Col 2 Header Overview Content',4,$page->id,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...',34)  }}
{{  addSection('Section 6 Col 2 Link',3,$page->id,'#',35)  }}

{{  addSection('Button Text Section 6',3,$page->id,'Read All News',36)  }}
{{  addSection('Button Link Section 6',3,$page->id,'news-overview',37)  }} --}}


{{  addSection('Section 7 Content',1,$page->id,'<h3>Get In Touch</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>',38)  }}

{{  addSection('Button Text Section 7',3,$page->id,'Contact',39)  }}
{{  addSection('Button Link Section 7',3,$page->id,'contact-us',40)  }}

{{  addSection('Button Text Section 7.1',3,$page->id,'(858) 224-7360',41)  }}
{{  addSection('Button Link Section 7.1',3,$page->id,'#',42)  }}



