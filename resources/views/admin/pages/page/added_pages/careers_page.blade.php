
{{--
    add section info
    
    addSection($name, $type, $pages, $value = '', $pos)
    Type EDITOR = 1;
    Type ATTACHMENT = 2;
    Type FORM = 3;
    Type TEXTAREA = 4;

--}}

{{-- {{  addSection('Service 1 title',3,1,'',1)  }}
{{  addSection('Service 1 description',3,1,'',2)  }}

{{  addSection('Service 2 title',3,1,'',3)  }}
{{  addSection('Service 2 description',3,1,'',4)  }}

{{  addSection('Service 3 title',3,1,'',5)  }}
{{  addSection('Service 3 description',3,1,'',6)  }}

{{  addSection('Service 4 title',3,1,'',7)  }}
{{  addSection('Service 4 description',3,1,'',8)  }}

{{  addSection('Service 5 title',3,1,'',9)  }}
{{  addSection('Service 5 description',3,1,'',10)  }}

{{  addSection('Service 6 title',3,1,'',11)  }}
{{  addSection('Service 6 description',3,1,'',12)  }} --}}


{{-- {{  addSection('Service 1 title',3,$page->id,'',1)  }} --}}


{{  addSection('Image Section 1',2,$page->id,'public/images/team-banner.jpg',1)  }}
{{  addSection('Content Section 1',1,$page->id,'<h1>Careers</h1>
<h2>At Next Wave, we believe it’s the team that makes the difference.</h2>

<p>Let’s talk about what you’re passionate about and where you see yourself. Together we can explore the possibilities.</p>',2)  }}

{{  addSection('Section 2 Header',3,$page->id,'Open Positions',4)  }}

{{  addSection('Section 2 Item 1 Title',3,$page->id,'Front-End Software Developer',5)  }}
{{  addSection('Section 2 Item 1 Content',1,$page->id,'                        <h4>Job Description</h4>

<p>We are seeking Front-End Software Developers/ Engineers for involvement in UI/ UX/ and applications development. If you have interest, please apply so we can talk further.</p>
<ul>
    <li>Education in software engineering, computer science or like fields</li>
    <li>Ability to write HTML, JavaScript and C++</li>
    <li>Familiar with producing and maintaining a dynamic UI</li>
    <li>Optimize applications for speed and scalability</li>
    <li>Follow QA/ QC protocols and collaborate with team members</li>
</ul>',6)  }}

{{  addSection('Section 2 Item 2 Title',3,$page->id,'Back-end Software Developer',7)  }}
{{  addSection('Section 2 Item 2 Content',1,$page->id,'  <h4>Job Description</h4>

<p>We are seeking Back-End Software Developers/ Engineers to build server-side application logic, infrastructure software, database design, and future implementations for the software. If we have your interest, please apply so we talk further.</p>
<ul>
    <li>Education and involvement in a STEM field </li>
    <li>Experience in developing back-end data analytics software, and troubleshooting software and data issues</li>
    <li>Familiar with ticketing/ case management systems to document issues or new functionality, diligently track open-items and effectively resolve issues</li>
</ul>',8)  }}

{{  addSection('Section 2 Item 3 Title',3,$page->id,'Field Service Technician',9)  }}
{{  addSection('Section 2 Item 3 Content',1,$page->id,' <h4>Job Description</h4>

<p>We are seeking Field Service Engineers for the installation and troubleshooting Field Service Technician for industrial communications. A summary of some qualifications and responsibilities are listed below. If we have your interest, please apply so we talk further.</p>
<ul>
    <li>Experience with common industrial communications protocols (i.e., Modbus, RS-485, Ethernet, TCP/ IP and port forwarding, etc.)</li>
    <li>Familiar with 3-phase power usage and troubleshooting (277/ 480VAC).</li>
    <li>Install and troubleshoot dataloggers and Modbus mapping, and small industrial DC power supplies</li>
    <li>Follow common safety practices of industrial electrical field work, safely work at heights, use ladders, lifts, platforms, and to implement fall protection safety practices</li>
    <li>Effectively use a multimeter, identify cleared overcurrent protection devices (fuses, breakers, etc.)</li>
    <li>Travel</li>
</ul>',10)  }}

{{  addSection('Section 2 Item 4 Title',3,$page->id,'Support Specialist',11)  }}
{{  addSection('Section 2 Item 4 Content',1,$page->id,'  <h4>Job Description</h4>

<p>We are seeking Support Specialists to assist our growing customer base with coordinating new installation, technical issues and internal coordination efforts. A summary of some responsibilities and qualifications are listed below. If we have your interest, please apply so we talk further.</p>

<ul>
    <li>Experience with customer service roles (i.e., Tier I-III)</li>
    <li>Familiar with ticketing/ case management systems to document requests, diligently track open-items and effectively resolve issues</li>
    <li>Inbound customer inquiries, support technical issues (hardware and software) in real-time and promote culture of customer loyalty</li>
    <li>Identify problems and work with the team to address the concerns</li>
    <li>Initiative to run reports on open and closed issues to help document potential trends for process improvement strategies.</li>
</ul>',12)  }}

{{  addSection('Section 2 Item 5 Title',3,$page->id,'Customer Success Manager',13)  }}
{{  addSection('Section 2 Item 5 Content',1,$page->id,'<h4>Job Description</h4>

<p>We are seeking Customer Success Managers to oversee and manage the process of customer retention, manage our customer support team, develop best practices, and the overarching mission to increase customer loyalty. A summary of some responsibilities and qualifications are listed below. If we have your interest, please apply so we talk further.</p>

<ul>
    <li>Experience with customer success roles and managing customer and partner activities</li>
    <li>Lead customer retention efforts, mentor and recruit customer support specialists, and promote culture of customer loyalty</li>
    <li>Improve internal processes to remedy trends that may potentially impact the customer experience, and KPI tracking and reporting</li>
    <li>Involvement with the product roadmap</li>
    <li>Maintain regular communication with customers to be closer to downstream experiences with the platform and service</li>
    <li>Identify potential topics for news and blog releases which will interest existing and new potential customers</li>
</ul>',14)  }}

{{  addSection('Side Header',3,$page->id,'Apply Today!',15)  }}
{{  addSection('Side Content',1,$page->id,'<p>If you’re interested in hearing more about open positions at Next Wave, apply by clicking the button below. We look forward to meeting you.</p>
',16)  }}

{{  addSection('Side Button Text',3,$page->id,'Apply Now',17)  }}
{{  addSection('Side Button Link',3,$page->id,'#',18)  }}

