
{{--
    add section info
    
    addSection($name, $type, $pages, $value = '', $pos)
    Type EDITOR = 1;
    Type ATTACHMENT = 2;
    Type FORM = 3;
    Type TEXTAREA = 4;

--}}
{{  addSection('Image Section 1',2,$page->id,'public/images/homebanner-img_contact_us.jpg',1)  }}
{{  addSection('Content Section 1',1,$page->id,'<h1>Contact Us</h1>
<h2>How may we help you?</h2>',2)  }}

{{  addSection('Header-Content Section 2',1,$page->id,'<h3>Contact Information</h3>
<p>Do you need help from the Next Wave Support Team? Don’t hesitate to contact us. We look forward to hearing from you.</p>',3)  }}

{{  addSection('Section 2 Address',1,$page->id,' <p>Next Wave Energy Monitoring, Inc.<br/>
    <strong>3142 Tiger Run Court, #118 Carlsbad, CA 92010</strong></p>',4)  }}
{{  addSection('Section 2 Phone',3,$page->id,'(858) 775-3888 ',5)  }}
{{  addSection('Section 2 Email',3,$page->id,'support@nextwavemonitoring.com',6)  }}