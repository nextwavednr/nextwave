
{{--
    add section info
    
    addSection($name, $type, $pages, $value = '', $pos)
    Type EDITOR = 1;
    Type ATTACHMENT = 2;
    Type FORM = 3;
    Type TEXTAREA = 4;

--}}

{{  addSection('Image Section 1',2,$page->id,'public/images/news-overview-banner.jpg',1)  }}
{{  addSection('Content Section 1',1,$page->id,'<h1>News</h1>
<h2>What\'s new?<br/>Read our News and Tips</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>',2)  }}
{{-- 
{{  addSection('Button Text Section 1',3,$page->id,'Our Approach',3)  }}
{{  addSection('Button Link Section 1',3,$page->id,'about-us',4)  }} --}}

{{  addSection('Image Section 2',2,$page->id,'public/images/news01.jpg',3)  }}

{{  addSection('Content Section 2',1,$page->id,' <h3>News 1</h3>
<span class="meta-date">11/11/2021</span>
<span class="line"></span>

<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum...</p>',4)  }}

{{  addSection('Button Text Section 2',3,$page->id,'Read More',5)  }}
{{  addSection('Button Link Section 2',3,$page->id,'#',6)  }}

{{  addSection('Section 7 Content',1,$page->id,'<h3>Get In Touch</h3>

<p>Don’t hesitate to contact us to hear more about our performance monitoring and analytics software. We look forward to it.</p>
',38)  }}

{{  addSection('Button Text Section 7',3,$page->id,'Contact',39)  }}
{{  addSection('Button Link Section 7',3,$page->id,'contact-us',40)  }}

{{  addSection('Button Text Section 7.1',3,$page->id,'(858) 224-7360',41)  }}
{{  addSection('Button Link Section 7.1',3,$page->id,'#',42)  }}
