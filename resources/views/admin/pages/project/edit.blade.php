@extends('admin.layouts.base')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ route('admin.projects.index') }}">Projects</a></li>
        <li><span href="javascript:void(0)">Edit Project</span></li>
    </ul>
    <div class="row">
        {{  Form::open([
            'method' => 'PUT',
            'id' => 'edit-project',
            'route' => ['admin.projects.update', $project->id],
            'class' => 'form-horizontal ',
            'files' => TRUE
            ])
        }}
        <div class="col-md-12">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>Edit Project "{{$project->name}}"</strong></h2>
                </div>
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="project_name">Name</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="project_name" name="name"
                               value="{{  Request::old('name') ? : $project->name }}"
                               placeholder="Enter Project name..">
                        @if($errors->has('name'))
                            <span class="help-block animation-slideDown">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="project_slug">Slug</label>

                    <div class="col-md-9">
                        <input disabled type="text" class="form-control" id="project_slug" name="slug"
                               value="{{  Request::old('slug') ? : $project->slug }}"
                               placeholder="Enter Project slug..">
                        @if($errors->has('slug'))
                            <span class="help-block animation-slideDown">{{ $errors->first('slug') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('banner_image') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="project_banner_image">Banner Image</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    Choose File <input type="file" name="banner_image" style="display: none;">
                                </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if($errors->has('banner_image'))
                            <span class="help-block animation-slideDown">{{ $errors->first('banner_image') }}</span>
                        @endif
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                        <a href="{{ asset($project->banner_image) }}" class="zoom img-thumbnail" style="cursor: default !important;" data-toggle="lightbox-image">
                            <img src="{{ $project->banner_image != '' ? asset($project->banner_image) : '' }}"
                                 alt="{{ $project->banner_image != '' ? asset($project->banner_image) : '' }}"
                                 class="img-responsive center-block" style="max-width: 100px;">
                        </a>
                        <br>
                        <a href="javascript:void(0)" class="btn btn-xs btn-danger remove-image-btn"
                           style="display: {{ $project->banner_image != '' ? '' : 'none' }};"><i class="fa fa-trash"></i> Remove</a>
                        <input type="hidden" name="remove_banner_image" class="remove-image" value="0">
                    </div>
                </div>

                <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="product_content">Service Category</label>

                    <div class="col-md-9">

                        @php
                            $array_cateogry = [];
                            $categs = json_decode($project->category_service_arr);
                            if($categs != null)
                            {
                                $array_cateogry = $categs;
                            }
                        @endphp
            
                    <select name="service_categ[]" id="service_categ" class="form-control select2" multiple="multiple">
                
                        {{-- @foreach ($array_cateogry as $category)
                            <option selected value="{{$category}}">{{$category}}</option>
                        @endforeach --}}
                        
                    @foreach (\App\Models\Service::all() as $service)
                            <option {{in_array($service->id,$array_cateogry) ? 'selected' : ''}} data="{{$service->id}}" value="{{$service->id}}">{{$service->name}}</option>
                        @endforeach
            
                    </select>
            
                    </div>
                </div>

                <!-- <div class="form-group file-container {{ $errors->has('file') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="project_file">File</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    Choose File <input type="file" name="file" style="display: none;">
                                </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if($errors->has('file'))
                            <span class="help-block animation-slideDown">{{ $errors->first('file') }}</span>
                        @endif
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                        <a target="_blank" href="{{ asset($project->file) }}" class="file-anchor">
                            {{ $project->file }}
                        </a>
                        <br>
                        <a href="javascript:void(0)" class="btn btn-xs btn-danger remove-file-btn"
                           style="display: {{ $project->file != '' ? '' : 'none' }};"><i class="fa fa-trash"></i> Remove</a>
                        <input type="hidden" name="remove_file" class="remove-file" value="0">
                    </div>
                </div> -->

                <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="project_content">Content</label>

                    <div class="col-md-9">
                    <textarea id="project_content" name="content" rows="9" class="form-control ckeditor"
                              placeholder="Enter Project content..">{!! Request::old('content') ? : $project->content !!}</textarea>
                        @if($errors->has('content'))
                            <span class="help-block animation-slideDown">{{ $errors->first('content') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                        <label class="col-md-3 control-label" for="file">Add Image in Gallery</label>
                        <div class="col-md-9">
                            <div class='btn btn-primary'>
                                Choose Image
                                <input type='file' class='form-control' name='incrementPhoto[]' multiple>
                            </div>
                        </div>
                        
                        @if($errors->has('incrementPhoto.*'))
                        <span style="color: red" class="help-block animation-slideDown">{{ $errors->first('incrementPhoto.*') }}</span>
                        @endif
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="file">Uploaded Images</label>
                    <div class="col-md-9">
                    {{-- <div class="col-md-9"> --}}
                    <div class="row">
                            @foreach ($project->gallery as $index => $images)

                            <div class="col-sm-2"><label>Pos: {{$index+1}}</label>
                                    <input type="hidden" name="image_id[]" value="{{$images->id}}">
                                    <input type="hidden" name="image_fixed_pos[]" value="{{$index+1}}">
                                    <div ondrop="drop(event)" ondragover="allowDrop(event)">
                                        <div style="cursor: pointer" draggable="true" ondragstart="drag(event)" id="image_gal_div{{$images->id}}">
                                            <small>{{'<Drag Here>'}}<br>Current pos: {{$index+1}}</small>
                                                <input type="hidden" name="image_fix_current[]" value="{{($index+1).'_'.$images->id}}">
                                                <a href="{{ asset($images->image) }}" class="zoom img-thumbnail" style="cursor: default !important; pointer-events: none;" data-toggle="lightbox-image">
                                                    <img src="{{ $images->image != '' ? asset($images->image) : '' }}"
                                                         alt="{{ $images->image != '' ? asset($images->image) : '' }}"
                                                         class="img-responsive center-block" style="max-width: 100%; max-height: 100px !important">
                                                </a>
                                                <br>
                                                <a href="javascript:void(0)" class="btn btn-xs btn-danger remove_image_btn_gallery" val="{{$images->id}}"><i class="fa fa-trash"></i> Remove</a>
                                                <input type="hidden" name="remove_image_gallery[]" id="remove-image-gallery{{$images->id}}" value="nan">
                                            </div>
                                    </div>
                                </div>
            
                            @endforeach
                    </div>
                    {{-- </div> --}}
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">Is Active?</label>

                    <div class="col-md-9">
                        <label class="switch switch-primary">
                            <input type="checkbox" id="is_active" name="is_active"
                                   value="1" {{ Request::old('is_active') ? : ($project->is_active ? 'checked' : '') }}>
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <a href="{{ route('admin.projects.index') }}" class="btn btn-sm btn-warning">Cancel</a>
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            @include('admin.pages.page.meta_fields',['seo_meta_fields'=>$project->seoMeta,'page'=>$project])
        </div>
        {{ Form::close() }}
    </div>
@endsection

@push('extrascripts')
    <script>
        $('.select2').select2();
    </script>
    <script type="text/javascript" src="{{ asset('public/js/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/libraries/projects.js') }}"></script>
    <script>

        $(document).on('click','.remove_image_btn_gallery',function(){

        var image_id = $(this).attr('val');
        $("#image_gal_div"+image_id).hide();
        $('#remove-image-gallery'+image_id).val(image_id);
        console.log(image_id);

        });

        function allowDrop(ev) {
            ev.preventDefault();
        }

        function drag(ev) {
            ev.dataTransfer.setData("text", ev.target.id);
        }

        function drop(ev) {
            // ev.preventDefault();
            // var data = ev.dataTransfer.getData("text");
            // ev.target.appendChild(document.getElementById(data));

            ev.preventDefault();
            var src = document.getElementById(ev.dataTransfer.getData("text"));
            var srcParent = src.parentNode;
            var tgt = ev.currentTarget.firstElementChild;

            ev.currentTarget.replaceChild(src, tgt);
            srcParent.appendChild(tgt);
        }

    </script>
@endpush
