@extends('admin.layouts.base')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ route('admin.projects.index') }}">Projects</a></li>
        <li><span href="javascript:void(0)">View Project</span></li>
    </ul>
    <div class="content-header">
        <div class="header-section">
            <h1>{{ $project->name }}</h1>
            <h5>{{ $project->slug }}</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <div class="block block-alt-noborder">
                <article>
                    <h3 class="sub-header text-center"><strong> {{ $project->created_at->format('F d, Y') }} </strong>
                        <div class="btn-group btn-group-xs pull-right">
                            @if (auth()->user()->can('Update Project'))
                                <a href="{{ route('admin.projects.edit', $project->id) }}"
                                   data-toggle="tooltip"
                                   title=""
                                   class="btn btn-default"
                                   data-original-title="Edit"><i class="fa fa-pencil"></i> Edit</a>
                            @endif
                            @if (auth()->user()->can('Delete Project'))
                                <a href="javascript:void(0)" data-toggle="tooltip"
                                   title=""
                                   class="btn btn-xs btn-danger delete-project-btn"
                                   data-original-title="Delete"
                                   data-project-id="{{ $project->id }}"
                                   data-project-route="{{ route('admin.projects.delete', $project->id) }}">
                                    <i class="fa fa-times"> Delete</i>
                                </a>
                            @endif
                        </div>
                    </h3>

                    <img src="{{ asset($project->banner_image) }}" alt="{{ $project->banner_image }}" class="img-responsive center-block" style="max-width: 100px;">

                    <p>{!! $project->content !!}</p>
                </article>
            </div>
        </div>
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/projects.js') }}"></script>
@endpush