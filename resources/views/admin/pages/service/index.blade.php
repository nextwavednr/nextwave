@extends('admin.layouts.base')

@section('content')
    @if (auth()->user()->can('Create Service'))
        <div class="row text-center">
            <div class="col-sm-12 col-lg-12">
                <a href="{{ route('admin.services.create') }}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background">
                        <h4 class="widget-content-light">
                            <strong>Add New</strong>
                            Service
                        </h4>
                    </div>
                    <div class="widget-extra-full">
                        <span class="h2 text-primary animation-expandOpen">
                            <i class="fa fa-plus"></i>
                        </span>
                    </div>
                </a>
            </div>
        </div>
    @endif
    <div class="block full">
        <div class="block-title">
            <h2>
                <i class="fa fa-newspaper-o sidebar-nav-icon"></i>
                <strong>Services</strong>
            </h2>
        </div>
        <div class="alert alert-info alert-dismissable service-empty {{$services->count() == 0 ? '' : 'johnCena' }}">
            <i class="fa fa-info-circle"></i> No Services found.
        </div>
        <div class="table-responsive {{$services->count() == 0 ? 'johnCena' : '' }}">
            <table id="services-table" class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr role="row">
                    <th class="text-center">
                        ID
                    </th>
                    <th class="text-center">
                        Name
                    </th>
                    <th class="text-left">
                        Slug
                    </th>
                    <th class="text-left">
                        Is Active
                    </th>
                    <th class="text-left">
                        Content
                    </th>
                    <th class="text-center">
                        Date Created
                    </th>
                    <th class="text-center">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($services as $service)
                    <tr data-service-id="{{$service->id}}">
                        <td class="text-center"><strong>{{ $service->id }}</strong></td>
                        <td class="text-center"><strong>{{ $service->name }}</strong></td>
                        <td class="text-left">
                            @if($service->slug && $service->slug != '')
                                <a target="_blank" href="{{ url('service-details/'.$service->slug) }}">{{ url('service-details/'.$service->slug) }}</a>
                            @endif
                        </td>
                        <td class="text-center"><strong>{{ $service->is_active == '1' ? 'Active' : 'Inactive' }}</strong></td>
                        <td class="text-left">{!! str_limit(strip_tags($service->content), 50) !!}</td>
                        <td class="text-center">{{ $service->created_at->format('F d, Y') }}</td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                <!-- @if (auth()->user()->can('Read Service'))
                                    <a href="{{ route('admin.services.show', $service->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="View"><i class="fa fa-eye"></i></a>
                                @endif -->
                                @if (auth()->user()->can('Update Service'))
                                    <a href="{{ route('admin.services.edit', $service->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                @endif
                                @if (auth()->user()->can('Delete Service'))
                                    <a href="javascript:void(0)" data-toggle="tooltip"
                                       title=""
                                       class="btn btn-xs btn-danger delete-service-btn"
                                       data-original-title="Delete"
                                       data-service-id="{{ $service->id }}"
                                       data-service-route="{{ route('admin.services.delete', $service->id) }}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/services.js') }}"></script>
@endpush