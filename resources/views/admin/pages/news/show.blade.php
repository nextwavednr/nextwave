@extends('admin.layouts.base')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ route('admin.news.index') }}">News</a></li>
        <li><span href="javascript:void(0)">View News</span></li>
    </ul>
    <div class="content-header">
        <div class="header-section">
            <h1>{{ $news->name }}</h1>
            <h5>{{ $news->slug }}</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <div class="block block-alt-noborder">
                <article>
                    <h3 class="sub-header text-center"><strong> {{ $news->created_at->format('F d, Y') }} </strong>
                        <div class="btn-group btn-group-xs pull-right">
                            @if (auth()->user()->can('Update News'))
                                <a href="{{ route('admin.news.edit', $news->id) }}"
                                   data-toggle="tooltip"
                                   title=""
                                   class="btn btn-default"
                                   data-original-title="Edit"><i class="fa fa-pencil"></i> Edit</a>
                            @endif
                            @if (auth()->user()->can('Delete News'))
                                <a href="javascript:void(0)" data-toggle="tooltip"
                                   title=""
                                   class="btn btn-xs btn-danger delete-news-btn"
                                   data-original-title="Delete"
                                   data-news-id="{{ $news->id }}"
                                   data-news-route="{{ route('admin.news.delete', $news->id) }}">
                                    <i class="fa fa-times"> Delete</i>
                                </a>
                            @endif
                        </div>
                    </h3>

                    <img src="{{ asset($news->banner_image) }}" alt="{{ $news->banner_image }}" class="img-responsive center-block" style="max-width: 100px;">

                    <p>{!! $news->content !!}</p>
                </article>
            </div>
        </div>
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/news.js') }}"></script>
@endpush