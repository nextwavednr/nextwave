@extends('admin.layouts.base')

@section('content')
    @if (auth()->user()->can('Create News'))
        <div class="row text-center">
            <div class="col-sm-12 col-lg-12">
                <a href="{{ route('admin.news.create') }}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background">
                        <h4 class="widget-content-light">
                            <strong>Add New</strong>
                            News
                        </h4>
                    </div>
                    <div class="widget-extra-full">
                        <span class="h2 text-primary animation-expandOpen">
                            <i class="fa fa-plus"></i>
                        </span>
                    </div>
                </a>
            </div>
        </div>
    @endif
    <div class="block full">
        <div class="block-title">
            <h2>
                <i class="fa fa-newspaper-o sidebar-nav-icon"></i>
                <strong>News</strong>
            </h2>
        </div>
        <div class="alert alert-info alert-dismissable news-empty {{$news->count() == 0 ? '' : 'johnCena' }}">
            <i class="fa fa-info-circle"></i> No News found.
        </div>
        <div class="table-responsive {{$news->count() == 0 ? 'johnCena' : '' }}">
            <table id="news-table" class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr role="row">
                    <th class="text-center">
                        ID
                    </th>
                    <th class="text-center">
                        Name
                    </th>
                    <th class="text-left">
                        Slug
                    </th>
                    <th class="text-left">
                        Content
                    </th>
                    <th class="text-center">
                        Date Created
                    </th>
                    <th class="text-center">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($news as $news)
                    <tr data-news-id="{{$news->id}}">
                        <td class="text-center"><strong>{{ $news->id }}</strong></td>
                        <td class="text-center"><strong>{{ $news->name }}</strong></td>
                        <td class="text-left">
                            @if($news->slug && $news->slug != '')
                                <a target="_blank" href="{{ url('news-details/'.$news->slug) }}">{{ url('news-details/'.$news->slug) }}</a>
                            @endif
                        </td>
                        <td class="text-left">{!! str_limit(strip_tags($news->content_1), 50) !!}</td>
                        <td class="text-center">{{ $news->created_at->format('F d, Y') }}</td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                @if (auth()->user()->can('Read News'))
                                    {{-- <a href="{{ route('admin.news.show', $news->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="View"><i class="fa fa-eye"></i></a> --}}
                                @endif
                                @if (auth()->user()->can('Update News'))
                                    <a href="{{ route('admin.news.edit', $news->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                @endif
                                @if (auth()->user()->can('Delete News'))
                                    <a href="javascript:void(0)" data-toggle="tooltip"
                                       title=""
                                       class="btn btn-xs btn-danger delete-news-btn"
                                       data-original-title="Delete"
                                       data-news-id="{{ $news->id }}"
                                       data-news-route="{{ route('admin.news.delete', $news->id) }}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/news.js') }}"></script>
@endpush