@extends('admin.layouts.base')

@section('content')
    {{-- @if (auth()->user()->can('Create Career')) --}}
        <div class="row text-center">
            <div class="col-sm-12 col-lg-12">
                <a href="{{ route('admin.careers.create') }}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background">
                        <h4 class="widget-content-light">
                            <strong>Add New</strong>
                            Career
                        </h4>
                    </div>
                    <div class="widget-extra-full">
                        <span class="h2 text-primary animation-expandOpen">
                            <i class="fa fa-plus"></i>
                        </span>
                    </div>
                </a>
            </div>
        </div>
    {{-- @endif --}}
    <div class="block full">
        <div class="block-title">
            <h2>
                <i class="fa fa-newspaper-o sidebar-nav-icon"></i>
                <strong>Careers</strong>
            </h2>
        </div>
        <div class="alert alert-info alert-dismissable career-empty {{$careers->count() == 0 ? '' : 'johnCena' }}">
            <i class="fa fa-info-circle"></i> No Careers found.
        </div>
        <div class="table-responsive {{$careers->count() == 0 ? 'johnCena' : '' }}">
            <table id="careers-table" class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr role="row">
                    <th class="text-center">
                        ID
                    </th>
                    <th class="text-center">
                        Name
                    </th>
                    <th class="text-left">
                        Slug
                    </th>
                    <th class="text-left">
                        Content
                    </th>
                    <th class="text-center">
                        Date Created
                    </th>
                    <th class="text-center">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($careers as $career)
                    <tr data-career-id="{{$career->id}}">
                        <td class="text-center"><strong>{{ $career->id }}</strong></td>
                        <td class="text-center"><strong>{{ $career->name }}</strong></td>
                        <td class="text-left">
                            @if($career->slug && $career->slug != '')
                                <a target="_blank" href="{{ add_http($career->slug) }}">{{ add_http($career->slug) }}</a>
                            @endif
                        </td>
                        <td class="text-left">{!! str_limit(strip_tags($career->content), 50) !!}</td>
                        <td class="text-center">{{ $career->created_at->format('F d, Y') }}</td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                {{-- @if (auth()->user()->can('Read Career'))
                                    <a href="{{ route('admin.careers.show', $career->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="View"><i class="fa fa-eye"></i></a>
                                @endif --}}
                                {{-- @if (auth()->user()->can('Update Career')) --}}
                                    <a href="{{ route('admin.careers.edit', $career->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                {{-- @endif --}}
                                {{-- @if (auth()->user()->can('Delete Career')) --}}
                                    <a href="javascript:void(0)" data-toggle="tooltip"
                                       title=""
                                       class="btn btn-xs btn-danger delete-career-btn"
                                       data-original-title="Delete"
                                       data-career-id="{{ $career->id }}"
                                       data-career-route="{{ route('admin.careers.delete', $career->id) }}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                {{-- @endif --}}
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/careers.js') }}"></script>
@endpush