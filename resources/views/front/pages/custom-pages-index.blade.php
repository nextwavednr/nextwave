@extends('front.layouts.base')

@section('content')
    @if (!empty($page))
        <?php $item = $page; ?>
        {{-- need to be updated to live slugs or by page type --}}
        @if ($page['slug'] == 'home')
            @include('front.pages.custom-page.home')
        @elseif ($page['slug'] == 'contact-us')
            @include('front.pages.custom-page.contact-us')
        @elseif ($page['slug'] == 'careers')
            @include('front.pages.custom-page.careers')
        @elseif ($page['slug'] == 'about-us')
            @include('front.pages.custom-page.about-us')
        @elseif ($page['slug'] == 'team-overview')
            @include('front.pages.custom-page.team-overview')

        {{-- @elseif ($page['slug'] == 'team-details')
            @include('front.pages.custom-page.team-details') --}}

        @elseif ($page['slug'] == 'news-overview')
            @include('front.pages.custom-page.news-overview')
        {{-- @elseif ($page['slug'] == 'news-details')
            @include('front.pages.custom-page.news-details') --}}


        @elseif ($page['slug'] == 'tutorials')
            @include('front.pages.custom-page.tutorials')
        @elseif ($page['slug'] == 'landing-page')
            @include('front.pages.custom-page.landing-page')

        

        @else
            @include('front.pages.custom-page.default-page')
        @endif
    @else
        @include('front.pages.custom-page.home')
    @endif
@endsection
