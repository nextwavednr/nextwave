<section class="page page--tutorials">
    @include('front.layouts.sections.header')

    <main class="main-content">
        <section class="innerpage-banner-circle">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6 innerpage-banner-circle__content">
                        <h1>Tutorials</h1>
                        <h2>Tutorials</h2>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>

                    <div class="col-lg-6 innerpage-banner-circle__image">
                        <img src="{{asset('public/images/news-overview-banner.jpg')}}">
                    </div>
                </div>
            </div>
        </section>

        <section class="tutorials">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6 tutorials__list">
                        <div class="tutorials__list--video">
                            <iframe src="https://www.youtube.com/embed/tgbNymZ7vqY" height="500" width="100%" frameBorder="0"></iframe>
                        </div>

                        <div class="tutorials__list--details">
                            <h3>Tutorial 1</h3>
                        </div>
                    </div>

                    <div class="col-lg-6 tutorials__list">
                        <div class="tutorials__list--video">
                            <iframe src="https://www.youtube.com/embed/tgbNymZ7vqY" height="500" width="100%" frameBorder="0"></iframe>
                        </div>

                        <div class="tutorials__list--details">
                            <h3>Tutorial 1</h3>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
         
    </main>
    @include('front.layouts.sections.footer')
</section>