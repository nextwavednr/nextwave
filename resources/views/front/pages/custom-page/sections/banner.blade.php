<section class="banner image-bg">
    <img src="{!! !empty($item) && !empty($item->banner_image) ?
    asset($item->banner_image) :
    asset('public/images/dogandrooster_full_bg.jpg') !!}">
    <div class="sub-banner__wrapper container ">
        <div class="sub-banner__wrapper--row row mx-auto">
            <div class="col-md-6 sub-banner__content sub-banner__items">
                {{-- <h1>{{ !empty($item) ? isset($item->name) ? $item->name : '' : '' }}</h1> --}}

                <h1>{!! !empty($item) ? isset($item->name) ? $item->name : '' : '' !!}</h1>
                {{-- <p>
                    {!! !empty($item) && !empty($item->banner_image) ?
                    nl2br($item->content) : '' !!}</p> --}}
            </div>
            <div class="col-md-6 sub-banner__content sub-banner__items">
            </div>
        </div>
    </div>
</section>