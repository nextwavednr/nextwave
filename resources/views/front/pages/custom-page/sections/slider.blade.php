<section class="banner">
    <div class="row">
        <div class="col-lg-5 banner__content">
            <div class="banner__slick">
                <div class="banner__item">

                    {!!section($page,'Content Section 1')!!}

                    {{-- <h1>Renewable Energy Performance Monitoring</h1>
                    <h2>Actionable Data + Market-Leading Customer Service</h2>

                    <p>System monitoring is the core of a renewable resource which is why our platform is centered on methodical data management and an enhanced user experience.</p> --}}

                    <!-- <a href="{{url('about-us')}}" class="btn btn--yellow">Our Approach <img src="{{asset('public/images/right-arrow.png')}}"></a> -->

                    <a href="{{url(''.section($page,'Button Link Section 1').'')}}" class="btn btn--yellow">{{section($page,'Button Text Section 1')}} <img src="{{asset('public/images/right-arrow.png')}}"></a>

                </div>
            </div>
        </div>

        <div class="col-lg-7 banner__image">
            <img src="{{asset(''.section($page,'Image Section 1').'')}}">
        </div>
    </div>
</section>