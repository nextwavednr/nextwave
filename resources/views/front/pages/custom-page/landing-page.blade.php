@push('extrastylesheets')
<style>

    section.homepage .energy-vitals:before{
        background-image: url("{{ section($page,'Image Section 2') }}");
    }

    section.homepage .quality-engineers:before{
        background-image: url("{{ section($page,'Image Section 3') }}");
    }

</style>
<section class="page--landingpage">
    @include('front.layouts.sections.header')

    
    
    <main id="main" class="main-content">
        {{-- Hero --}}
        <section class="banner">
            <div class="row">
                <div class="col-lg-5 banner__content">
                    <div class="banner__slick">
                        <div class="banner__item">
                            <h2>Renewable Energy Made Easy</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a href="#" class="btn btn--yellow">Contact Us Today <img src="{{asset('public/images/right-arrow.png')}}"></a> 
                        </div>
                    </div>
                </div>
        
                <div class="col-lg-7 banner__image">
                    <img src="{{'public/images/landing-page_img.jpg'}}">
                </div>
            </div>
        </section>

        <section class="section--clients">
            <div class="wrapper">
                <div class="heading">
                    <h2>Why companies love us</h2>
                </div>
                <div class="section--clients__content">
                    <div class="client--list">
                        <div class="client--list__item">
                            <a href="#">
                                <img src="{{'public/images/logo-1.png'}}">
                            </a>
                        </div>
                        <div class="client--list__item">
                            <a href="#">
                                <img src="{{'public/images/logo-2.png'}}">
                            </a>
                        </div>
                        <div class="client--list__item">
                            <a href="#">
                                <img src="{{'public/images/logo-3.png'}}">
                            </a>
                        </div>
                        <div class="client--list__item">
                            <a href="#">
                                <img src="{{'public/images/logo-4.png'}}">
                            </a>
                        </div>
                        <div class="client--list__item">
                            <a href="#">
                                <img src="{{'public/images/logo-5.png'}}">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="technologies">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6 technologies__empty"></div>
                    <div class="col-lg-6 technologies__content">
                        <h3>Best-in-class technologies</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                        <p>Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo llamco labori consequat. </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="section--mission">
            <div class="wrapper">
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <h4>Our mission is to simplify this high volume of data and focus on meaningful analytics </h4>
                    </div>
                    <div class="col-lg-5 text-lg-right text-center">
                        <a href="#" class="btn btn--yellow text-center">Contact</a>
                    </div>
                </div>
            </div>
        </section>

        <section class="hands-on-services">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6 hands-on-services__content">
                        <h3>Hands on Services</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        <a href="#" class="btn btn--yellow text-center">Tutorials</a>
                    </div>
                    <div class="col-lg-6 hands-on-services__image">
                        <img src="{{asset('public/images/landing-page_hand-on-service.jpg')}}">
                    </div>
                </div>
            </div>
        </section>

        {{-- We are Power Quality Engineers --}}
        <section class="section--quality-engineers">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6">

                    </div>
                    <div class="col-lg-6">
                        <div class="text-holder">
                            <h2>We Are Power Quality Engineers</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                            <a href="#" class="btn btn--yellow text-center">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {{-- Best in class Technologies --}}
        <section class="section--best-in-class">
            <div class="wrapper">
                <div class="heading">
                    <h2>Best-in-class technologies</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</p>
                </div>
                <div class="section--best-in-class__content">
                    <div class="class--list row">
                        <div class="col-lg-4 class--list__item">
                            <div class="icon-holder">
                                <img src="{{asset('public/images/class-icon-1.png')}}" alt="">
                            </div>
                            <div class="text-holder">
                                <h3>The Hub For Business Process</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do aeiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                        <div class="col-lg-4 class--list__item">
                            <div class="icon-holder">
                                <img src="{{asset('public/images/class-icon-2.png')}}" alt="">
                            </div>
                            <div class="text-holder">
                                <h3>Data Curation</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do aeiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                        <div class="col-lg-4 class--list__item">
                            <div class="icon-holder">
                                <img src="{{asset('public/images/class-icon-3.png')}}" alt="">
                            </div>
                            <div class="text-holder">
                                <h3>Portfolio Aggregation</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do aeiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                        
                    </div>
                    <div class="btn--group row">
                        <div class="col-lg-12 text-center">
                            <a href="#" class="btn btn--yellow text-center">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {{-- Get in touch --}}
        <section class="latest-update">
            <div class="wrapper">
                <div class="get-in-touch">
                    <div class="row">
                        <div class="col-lg-6 get-in-touch__content">

                            <h3>Get In Touch</h3>
                            <p>Don’t hesitate to contact us to hear more about our performance monitoring and analytics software. We look forward to it.</p>
                        </div>

                        <div class="col-lg-3 get-in-touch__buttons">
                            <a href="{{url('#')}}" class="btn btn--yellow">Contact</a>
                            <a href="tel:858-224-7360" class="btn btn--yellow">(858) 224-7360</a>
                        </div>

                        <div class="col-lg-3 get-in-touch__social-media">
                            <ul>
                                <li><a href="{{settings('SS0013')}}"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="{{settings('SS0015')}}"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>

    @include('front.layouts.sections.footer')
</section>
