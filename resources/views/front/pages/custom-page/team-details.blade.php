@extends('front.layouts.base')
@section('content')
<section class="page page--team-detail">
    @include('front.layouts.sections.header')
    
    <main class="main-content">
    
        <section class="innerpage-banner-team">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6 innerpage-banner-team__content">
                        <a href="{{url('team-overview')}}" class="back-to-team"><img src="{{asset('public/images/left-arrow.png')}}">Back To Team</a>
                        <h1>{{$team->name}}</h1>
                        <span class="line"></span>
                        <h2>{{$team->position}}</h2>
                    </div>

                    <div class="col-lg-6 innerpage-banner-team__image">
                        <img src="{{asset(''.$team->banner_image.'')}}">
                    </div>
                </div>
            </div>
        </section>

        <section class="bio">
            <div class="wrapper">
                <div class="row">
                    <h3>Bio</h3>

                    <p>{!!$team->bio!!} </p>
                    
                   {{-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                --}}
                </div>
            </div>
        </section>

        <section class="navigation">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6 navigation__prev">

                        @if(isset($team_prev))
                        <div class="navigation__prev--image">
                            <img src="{{asset(''.$team_prev->banner_image.'')}}">
                        </div>
                        <div class="navigation__prev--details">
                            <span><a href="{{url('team-details/'.$team_prev->slug.'')}}"><img src="{{asset('public/images/left-arrow.png')}}">Previous</a></span>

                            <h4><a href="{{url('team-details/'.$team_prev->slug.'')}}">{{$team_prev->name}}</a></h4>
                            <h5>{{$team_prev->position}}</h5>
                        </div>
                        @endif
                        
                    </div>

                    <div class="col-lg-6 navigation__next">

                        @if(isset($team_next))
                        <div class="navigation__next--details">
                            <span><a href="{{url('team-details/'.$team_next->slug.'')}}">Next <img src="{{asset('public/images/right-arrow.png')}}"></a></span>

                            <h4><a href="{{url('team-details/'.$team_next->slug.'')}}">{{$team_next->name}}</a></h4>
                            <h5>{{$team_next->position}}</h5>
                        </div>

                        <div class="navigation__next--image">
                            <img src="{{asset(''.$team_next->banner_image.'')}}">
                        </div>  
                        @endif

                    </div>
                </div>
            </div>
        </section>

        <section class="join-team">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6 join-team__empty"></div>

                    <div class="col-lg-6 join-team__details">
                        <h3>Join Our Team</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

                        <a href="#" class="btn btn--yellow">View Career</a>
                    </div>
                </div>
            </div>
        </section>
        
    </main>
    @include('front.layouts.sections.footer')
</section>
@endsection
