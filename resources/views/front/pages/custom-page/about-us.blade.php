@push('extrastylesheets')
<style>

    section.page--about-us .technologies:before{
        background-image: url("{{ section($page,'Image Section 2') }}");
    }

</style>
<section class="page page--about-us">
    @include('front.layouts.sections.header')

    <main class="main-content">
       
        <section class="innerpage-banner">
            <div class="row">
                <div class="col-lg-6 innerpage-banner__content">
                    {!!section($page,'Content Section 1')!!}

                    {{-- <h1>About Next Wave</h1>
                    <h2>Informed Decisions & Diagnostics Based on Real Data</h2>

                    <p>The Solution to Effectively Monitor Your Distributed Energy Resources (DER).</p> --}}
                </div>

                <div class="col-lg-6 innerpage-banner__image">
                    <img src="{{asset(''.section($page,'Image Section 1').'')}}">
                </div>
            </div>
        </section>

        <section class="mission">
            <div class="wrapper">
                <div class="row">
                    <!-- <h3>At the end of the day, once you strip down a grid-tied system, you are left with simple complimentary electrical components. Each of these circuit carrying components produce data; when organized and reported methodically it paints a complete picture of a system’s health vitals. </h3> -->
                    {{-- <h4>Our mission is to simplify this high volume of data and focus on meaningful analytics to identify root-cause and ultimately, reduce mean time to repair (MTTR).</h4> --}}

                    <h4>{{section($page,'Section 2 Header')}}</h4>
                
                </div>

                 <div class="row">
                    <div class="mission__video">
                        <img src="{{asset(''.section($page,'Section 2 Header-Image').'')}}">

                        @if (section($page,'Section 2 Header-Video-Link') != '')
                            <div class="mission__video--play-btn">
                                <a href="{{add_http(''.section($page,'Section 2 Header-Video-Link').'')}}" target="_blank"><i class="fas fa-play"></i></a>
                            </div>
                        @endif
                    </div>
                </div> 
        </section>

        <section class="technologies">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6 technologies__empty"></div>

                    <div class="col-lg-6 technologies__content">
                        {{-- <h3>Designed & Built by Industry Experts</h3>
                        <p>Next Wave’s experience with DER operations and real-world failures has allowed us to create a platform that provides monitoring, improved diagnostics, and fact-based decision making.</p>
                        <p>Tutorials: remove this option since it will give everyone an in-depth video of our UI and platform capabilities. We will add this to our Customer View UI that you already created.</p> --}}

                        {!!section($page,'Content Section 2')!!}

                        <!-- <a href="{{url(''.section($page,'Button Link Section 2').'')}}" class="btn btn--yellow">{{section($page,'Button Text Section 2')}}<img src="{{asset('public/images/right-arrow.png')}}"></a> -->
                    </div>
                </div>
            </div>
        </section>

        <section class="hands-on-services">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6 hands-on-services__content">
                        {!!section($page,'Content Section 3')!!}
                        {{-- <h3>Hands on Services</h3>
                        <p>Our customer service support understands how renewable energy facilities are intended to perform and the significance of Key Performance Indicators (KPI). This know-how provides an efficient approach to retrofitting existing facilities, new projects and supporting potential complications.</p> --}}

                        <a href="{{url(''.section($page,'Button Link Section 3').'')}}" class="btn btn--yellow">{{section($page,'Button Text Section 3')}}<img src="{{asset('public/images/right-arrow.png')}}"></a>
                    </div>

                    <div class="col-lg-6 hands-on-services__image">
                        <img src="{{asset(''.section($page,'Image Section 3').'')}}">
                    </div>
                </div>

                <div class="get-in-touch">
                    <div class="row">
                        <div class="col-lg-6 get-in-touch__content">
                            {!!section($page,'Section 7 Content')!!}
                            {{-- <h3>Get In Touch</h3>
                            <p>Don’t hesitate to contact us to hear more about our performance monitoring and analytics software. We look forward to it. </p> --}}
                       
                        </div>

                        <div class="col-lg-3 get-in-touch__buttons">
                            <a href="{{url(''.section($page,'Button Link Section 7.1').'')}}" class="btn btn--yellow">{{section($page,'Button Text Section 7.1')}}</a>
                            <a href="{{url(''.section($page,'Button Link Section 7.2').'')}}" class="btn btn--yellow">{{section($page,'Button Text Section 7.2')}}</a>
                        </div>

                        <div class="col-lg-3 get-in-touch__social-media">
                            <ul>
                                <li><a href="{{settings('SS0013')}}"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="{{settings('SS0015')}}"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </section>

        

    </main>
    @include('front.layouts.sections.footer')
</section>
