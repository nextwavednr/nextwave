@extends('front.layouts.base')

@section('content')

<section class="page page--news-details">
    @include('front.layouts.sections.header')
    
    <main class="main-content">
        
        <section class="innerpage-banner-team">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-12 innerpage-banner-team__content">
                        <a href="{{url('news-overview')}}" class="back-to-team"><img src="{{asset('public/images/left-arrow.png')}}">Back To News</a>
                        <h1>{{$news->name}}</h1>
                        <span class="line"></span>
                        {{-- <span class="meta-date">11/11/2021</span> --}}
                        <span class="meta-date">{{date('m/d/Y', strtotime($news->date))}}</span>
                    </div>
                </div>
            </div>
        </section>

        <section class="news">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-8 news__content">
                        <div class="news__content--featured-image">
                            <img src="{{asset(''.$news->banner_image.'')}}">
                        </div>

                        <div class="news__content--content">
                           
                            <h3>{{$news->content_1}}</h3>

                            {{-- <h3>Sample line text</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> --}}

                            {!!$news->content_2!!}
{{-- 
                            <h3>Sample List</h3>
                            <ul>
                                <li>One</li>
                                <li>Two</li>
                            </ul>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        --}}
                        </div>
                    </div>

                    <div class="col-lg-4 news__more-articles">
                        <h4>More Articles</h4>
                        
                        <ul>

                            @foreach ($news_random as $rnd)
                               
                                <li>
                                    <div class="news__more-articles--image">
                                        <img src="{{asset(''.$rnd->banner_image.'')}}">
                                    </div>

                                    <div class="news__more-articles--details">
                                        <span class="meta-date">{{date('m/d/Y', strtotime($rnd->date))}}</span>
                                        <h5><a href="{{url('news-details/'.$rnd->slug.'')}}">{{$rnd->name}}</a></h5>
                                    </div>
                                </li>

                            @endforeach

                            {{-- <li>
                                <div class="news__more-articles--image">
                                    <img src="{{asset('public/images/news02.jpg')}}">
                                </div>

                                <div class="news__more-articles--details">
                                    <span class="meta-date">10/12/2021</span>
                                    <h5>Lorem Ipsum</h5>
                                </div>
                            </li>

                            <li>
                                <div class="news__more-articles--image">
                                    <img src="{{asset('public/images/news05.jpg')}}">
                                </div>

                                <div class="news__more-articles--details">
                                    <span class="meta-date">10/12/2021</span>
                                    <h5>Lorem Ipsum</h5>
                                </div>
                            </li>

                            <li>
                                <div class="news__more-articles--image">
                                    <img src="{{asset('public/images/news03.jpg')}}">
                                </div>

                                <div class="news__more-articles--details">
                                    <span class="meta-date">10/12/2021</span>
                                    <h5>Lorem Ipsum</h5>
                                </div>
                            </li>

                            <li>
                                <div class="news__more-articles--image">
                                    <img src="{{asset('public/images/news04.jpg')}}">
                                </div>

                                <div class="news__more-articles--details">
                                    <span class="meta-date">10/12/2021</span>
                                    <h5>Lorem Ipsum</h5>
                                </div>
                            </li> --}}

                        </ul> 
                    </div>
                </div>

                <div class="get-in-touch">
                    <div class="row">
                        <div class="col-lg-6 get-in-touch__content">
                            <h3>Get In Touch</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>

                        <div class="col-lg-3 get-in-touch__buttons">
                            <a href="#" class="btn btn--yellow">Contact</a>
                            <a href="#" class="btn btn--yellow">(858) 224-7360</a>
                        </div>

                        <div class="col-lg-3 get-in-touch__social-media">
                            <ul>
                                <li><a href="{{settings('SS0013')}}"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="{{settings('SS0015')}}"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    </main>
    @include('front.layouts.sections.footer')
</section>
@endsection
