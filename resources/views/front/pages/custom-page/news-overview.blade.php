<section class="page page--news-overview">
    @include('front.layouts.sections.header')

    <main class="main-content">

        <section class="innerpage-banner-circle">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6 innerpage-banner-circle__content">

                        {!!section($page,'Content Section 1')!!}

                        {{-- <h1>News</h1>
                        <h2>What's new?<br/>Read our News and Tips</h2>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p> --}}

                    </div>

                    <div class="col-lg-6 innerpage-banner-circle__image">
                        <img src="{{asset(''.section($page,'Image Section 1').'')}}">
                    </div>
                </div>
            </div>
        </section>

        <section class="featured-news">
            <div class="wrapper">
                <div class="row">

                    @php

                        $news_feature_data = \App\Models\News::where('is_active',1)->where('is_feature',1)->get();

                        // $go = 0;

                        if(count($news_feature_data) != 0)
                        {
                            $news_feature = $news_feature_data[0];
                        }


                    @endphp

                    @if (count($news_feature_data) != 0)
                    <div class="col-lg-6 featured-news__image">
                        <img src="{{asset(''.$news_feature->banner_image.'')}}">
                    </div>


                    <div class="col-lg-6 featured-news__detail">

                        {{-- {!!section($page,'Content Section 2')!!} --}}

                        <h3>{{$news_feature->name}}</h3>
                        <span class="meta-date">{{date('m/d/Y', strtotime($news_feature->date))}}</span>
                        <span class="line"></span>

                        <p>{!!strip_tags(str_limit($news_feature->content_1, $limit = 400, $end = '...'))!!}</p>
{{-- 
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum...</p>  --}}

                        <a href="{{url('news-details/'.$news_feature->slug.'')}}" class="btn btn--yellow">READ MORE <img src="{{asset('public/images/right-arrow.png')}}"></a>

                        {{-- 
                        <a href="{{url(''.section($page,'Button Link Section 2').'')}}" class="btn btn--yellow">{{section($page,'Button Text Section 2')}} <img src="{{asset('public/images/right-arrow.png')}}"></a> --}}

                    </div>
                    @endif

                </div>
            </div>
        </section>

        <section class="news-list">
            <div class="wrapper">
                <div class="row">

                    @php

                        $news = \App\Models\News::where('is_active',1)->get();
                        $showd = 0;
                    @endphp

                    @foreach ($news as $new)
                
                    @php
                        if($new->is_feature == 1)
                        {
                            $showd = 1;
                        }
                    @endphp


                    @if($showd != 1)
                        <div class="col-lg-4 news-list__item">
                            <div class="news-list__item--image">
                                <img src="{{asset(''.$new->banner_image.'')}}">
                            </div>

                            <div class="news-list__item--detail">
                                <h3><a href="{{url('news-details/'.$new->slug.'')}}">{{$new->name}}</a></h3>
                                <span class="meta-date">{{date('m/d/Y', strtotime($new->date))}}</span>
                            </div>
                        </div>
                    @endif


                    @php
                 
                        $showd = 0;
                    
                    @endphp

                    @endforeach

                    {{-- <div class="col-lg-4 news-list__item">
                        <div class="news-list__item--image">
                            <img src="{{asset('public/images/news03.jpg')}}">
                        </div>

                        <div class="news-list__item--detail">
                            <h3>News 03</h3>
                            <span class="meta-date">11/11/2021</span>
                        </div>
                    </div>

                    <div class="col-lg-4 news-list__item">
                        <div class="news-list__item--image">
                            <img src="{{asset('public/images/news04.jpg')}}">
                        </div>

                        <div class="news-list__item--detail">
                            <h3>News 04</h3>
                            <span class="meta-date">11/11/2021</span>
                        </div>
                    </div>

                    <div class="col-lg-4 news-list__item">
                        <div class="news-list__item--image">
                            <img src="{{asset('public/images/news05.jpg')}}">
                        </div>

                        <div class="news-list__item--detail">
                            <h3>News 05</h3>
                            <span class="meta-date">11/11/2021</span>
                        </div>
                    </div>

                    <div class="col-lg-4 news-list__item">
                        <div class="news-list__item--image">
                            <img src="{{asset('public/images/news06.jpg')}}">
                        </div>

                        <div class="news-list__item--detail">
                            <h3>News 06</h3>
                            <span class="meta-date">11/11/2021</span>
                        </div>
                    </div>

                    <div class="col-lg-4 news-list__item">
                        <div class="news-list__item--image">
                            <img src="{{asset('public/images/news07.jpg')}}">
                        </div>

                        <div class="news-list__item--detail">
                            <h3>News 07</h3>
                            <span class="meta-date">11/11/2021</span>
                        </div>
                    </div>

                    <div class="col-lg-4 news-list__item">
                        <div class="news-list__item--image">
                            <img src="{{asset('public/images/news08.jpg')}}">
                        </div>

                        <div class="news-list__item--detail">
                            <h3>News 08</h3>
                            <span class="meta-date">11/11/2021</span>
                        </div>
                    </div> --}}

                </div>
            </div>
        </section>
    
        <section>
            <div class="wrapper">
                <div class="get-in-touch">
                    <div class="row">
                        <div class="col-lg-6 get-in-touch__content">

                            {!!section($page,'Section 7 Content')!!}

                           {{-- <h3>Get In Touch</h3>
                           <p>Don’t hesitate to contact us to hear more about our performance monitoring and analytics software. We look forward to it.</p> --}}
                       </div>

                       <div class="col-lg-3 get-in-touch__buttons">
                           <a href="{{url(''.section($page,'Button Link Section 7').'')}}" class="btn btn--yellow">{{section($page,'Button Text Section 7')}}</a>
                           <a href="{{section($page,'Button Link Section 7.1')}}" class="btn btn--yellow">{{section($page,'Button Text Section 7.1')}}</a>
                       </div>

                       <div class="col-lg-3 get-in-touch__social-media">
                           <ul>
                               <li><a href="{{settings('SS0013')}}"><i class="fab fa-twitter"></i></a></li>
                               <li><a href="{{settings('SS0015')}}"><i class="fab fa-linkedin-in"></i></a></li>
                           </ul>
                       </div>
                    </div>
                </div>
            </div>
        </section>
        
    </main>
    @include('front.layouts.sections.footer')
</section>