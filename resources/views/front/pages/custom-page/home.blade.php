@push('extrastylesheets')
<style>

    section.homepage .energy-vitals:before{
        background-image: url("{{ section($page,'Image Section 2') }}");
    }

    section.homepage .quality-engineers:before{
        background-image: url("{{ section($page,'Image Section 3') }}");
    }

</style>
<section class="homepage homepage--main">
    @include('front.layouts.sections.header')
    @include('front.pages.custom-page.sections.slider')

    <main id="main" class="main-content">
        <section class="energy-vitals">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6 energy-vitals__empty"></div>

                    <div class="col-lg-6 energy-vitals__content">

                         {!!section($page,'Content Section 2')!!}

                        {{-- <h3>We Deliver Substantive Energy Vitals:</h3>
                        <p>Next Wave Energy Monitoring is a renewable energy performance monitoring & data analytics platform with a mission to simplify the high volume of data and focus on meaningful analytics which will lead to root-cause and will reduce the Mean Time to Repair (MTTR).</p> --}}

                        {{-- <a href="{{url('about-us')}}" class="btn btn--yellow">Learn More<img src="{{asset('public/images/right-arrow.png')}}"></a> --}}
                        <a href="{{url(''.section($page,'Button Link Section 2').'')}}" class="btn btn--yellow">{{section($page,'Button Text Section 2')}}<img src="{{asset('public/images/right-arrow.png')}}"></a>


                    </div>
                </div>
            </div>
        </section>

        <section class="quality-engineers">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6 quality-engineers__content">

                         {!!section($page,'Content Section 3')!!}

                        {{-- <h3>Developed by Power Quality Engineers</h3>
                        <p>Next Wave is comprised of power quality and electrical engineers with decades of practical knowledge of power electronics and grid-tied energy resources. This expertise under one roof delivers a robust performance Data Acquisition System (DAS) with actionable data.</p> --}}

                        {{-- <a href="{{url('team')}}" class="btn btn--yellow">Our Team<img src="{{asset('public/images/right-arrow.png')}}"></a> --}}
                        <a href="{{url(''.section($page,'Button Link Section 3').'')}}" class="btn btn--yellow">{{section($page,'Button Text Section 3')}}<img src="{{asset('public/images/right-arrow.png')}}"></a>
                    </div>

                    <div class="col-lg-6 quality-engineers__empty"></div>
                </div>
            </div>
        </section>

        <section class="what-we-offer">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-12">
                     
                         {!!section($page,'Content Section 4')!!}

                        {{-- <h3>What We Offer</h3>
                        <p>Intuitive DAS platform for commercial and industrial, enterprise/ portfolio, and utility-scale applications.</p> --}}
                      
                        <div class="section--what-we-offer__slider what-we-offer__slider">

                            @foreach (\App\Models\HomeSlide::all() as $item)

                                <div class="what-we-offer__slide-item">
                                    <a href="{{$item->button_link}}">
                                        <div class="image">
                                            <img src="{{asset(''.$item->background_image.'')}}" alt="Service">
                                            <div class="text-content">

                                                <h4 class="service__name">{{$item->name}}</h4>
                                                {!!$item->content!!}
                                            
                                            </div>
                                        </div>
                                    </a>
                                </div>

                            @endforeach


                            {{-- <div class="what-we-offer__slide-item">
                                <a href="#">
                                    <div class="image">
                                        <img src="{{asset(''.section($page,'Section 4-1 Image').'')}}" alt="Service">
                                        <div class="text-content">

                                            <h4 class="service__name">{{section($page,'Section 4-1 Name')}}</h4>
                                            <p>{{section($page,'Section 4-1 Text')}}</p>
                                        
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="what-we-offer__slide-item">
                                <a href="#">
                                    <div class="image">
                                        <img src="{{asset(''.section($page,'Section 4-2 Image').'')}}" alt="Service">
                                        <div class="text-content">
                                            <h4 class="service__name">{{section($page,'Section 4-2 Name')}}</h4>
                                            <p>{{section($page,'Section 4-2 Text')}}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="what-we-offer__slide-item">
                                <a href="#">
                                    <div class="image">
                                        <img src="{{asset(''.section($page,'Section 4-3 Image').'')}}" alt="Service">
                                        <div class="text-content">
                                            <h4 class="service__name">{{section($page,'Section 4-3 Name')}}</h4>
                                            <p>{{section($page,'Section 4-3 Text')}} </p>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="what-we-offer__slide-item">
                                <a href="#">
                                    <div class="image">
                                        <img src="{{asset(''.section($page,'Section 4-4 Image').'')}}" alt="Service">
                                        <div class="text-content">
                                            <h4 class="service__name">{{section($page,'Section 4-4 Name')}}</h4>
                                            <p>{{section($page,'Section 4-4 Text')}}</p>
                                        </div>
                                    </div>
                                </a>
                            </div> 

                            <div class="what-we-offer__slide-item">
                                <a href="#">
                                    <div class="image">
                                        <img src="{{asset(''.section($page,'Section 4-5 Image').'')}}" alt="Service">
                                        <div class="text-content">
                                            <h4 class="service__name">{{section($page,'Section 4-5 Name')}}</h4>
                                            <p>{{section($page,'Section 4-5 Text')}}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>  --}}

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="why-choose-us">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-12">
                      
                         {!!section($page,'Content Section 5')!!}

                        {{-- <h3>Why Choose Us</h3>
                        <p>We’re not just a software company, we back it up with a customer-first philosophy. Call us and we’ll connect you with our customers to hear why they trust Next Wave Energy.</p> --}}
                    </div>

                    <div class="col-lg-3">
                        <div class="why-choose-us__item">
                            <div class="why-choose-us__item--front">

                                {{-- <img src="{{asset('public/images/wcu-icon01.png')}}">
                                <h4>Employee-Owned Company</h4> --}}

                                <img src="{{asset(''.section($page,'Section 5 Col 1 Icon').'')}}">
                                <h4>{{section($page,'Section 5 Col 1 Name')}}</h4> 
                            </div>

                            <div class="why-choose-us__item--back">
                                <h4>{{section($page,'Section 5 Col 1 Name')}}</h4>
                                <p>{{section($page,'Section 5 Col 1 Text')}}</p> 

                                {{-- <h4>Employee-Owned Company</h4>
                                <p>Our team’s invigorating passion to deliver the essential power and energy metrics to system owners is poised to disrupt the DAS industry.</p> --}}
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="why-choose-us__item">
                            <div class="why-choose-us__item--front">
                                {{-- <img src="{{asset('public/images/wcu-icon02.png')}}">
                                <h4>Innovation</h4> --}}

                                 <img src="{{asset(''.section($page,'Section 5 Col 2 Icon').'')}}">
                                <h4>{{section($page,'Section 5 Col 2 Name')}}</h4>
                           
                            </div>

                            <div class="why-choose-us__item--back">
                                {{-- <h4>Innovation</h4>
                                <p>Innovation at Next Wave is an ongoing effort. We see the evolving grid and greater efficiencies in electrical components as opportunities for perpetual advancement of our software.</p> --}}

                                 <h4>{{section($page,'Section 5 Col 2 Name')}}</h4>
                                <p>{{section($page,'Section 5 Col 2 Text')}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="why-choose-us__item">
                            <div class="why-choose-us__item--front">
                                {{-- <img src="{{asset('public/images/wcu-icon03.png')}}">
                                <h4>Sensible UI/ UX</h4> --}}

                                 <img src="{{asset(''.section($page,'Section 5 Col 3 Icon').'')}}">
                                <h4>{{section($page,'Section 5 Col 3 Name')}}</h4> 
                            </div>

                            <div class="why-choose-us__item--back">
                                {{-- <h4>Sensible UI/ UX</h4>
                                <p>Next Wave’s software engineers & designers have developed a user interface and experience that providers users a top-level control of a transparent platform.</p> --}}

                                 <h4>{{section($page,'Section 5 Col 3 Name')}}</h4>
                                <p>{{section($page,'Section 5 Col 3 Text')}}</p> 
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="why-choose-us__item">
                            <div class="why-choose-us__item--front">
                                {{-- <img src="{{asset('public/images/wcu-icon04.png')}}"> --}}
                                {{-- <h4>Customer Support</h4> --}}

                                 <img src="{{asset(''.section($page,'Section 5 Col 4 Icon').'')}}">
                                <h4>{{section($page,'Section 5 Col 4 Name')}}</h4> 
                            </div>

                            <div class="why-choose-us__item--back">
                                {{-- <h4>Customer Support</h4>
                                <p>We strive to provide seamless integration and ongoing support for host-customers, portfolio asset owners and financial participants.</p> --}}

                                 <h4>{{section($page,'Section 5 Col 4 Name')}}</h4>
                                <p>{{section($page,'Section 5 Col 4 Text')}}</p> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {{-- news module --}}
        <section class="latest-update">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>{{section($page,'Header Text Section 6')}}</h3>
                    </div>
                </div>

                @php

                    $news = \App\Models\News::where('is_active',1)->limit(2)->get();

                @endphp

                <div class="row">

                    @foreach ($news as $single_news)
                        <div class="col-lg-6 latest-update__item">
                            <div class="row">
                                <div class="col-lg-6 latest-update__item--image">
                                    <img src="{{asset(''.$single_news->banner_image.'')}}">
                                </div>

                                <div class="col-lg-6 latest-update__item--content">
                                    <h4><a href="{{url('news-details/'.$single_news->slug.'')}}">{{$single_news->name}}</a></h4>
                                    <p>{{ strip_tags(str_limit($single_news->content_1, $limit = 200, $end = '...')) }}</p>

                                    <a href="{{url('news-details/'.$single_news->slug.'')}}" class="news-btn"><img src="{{asset('public/images/right-arrow-white.png')}}"></a>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    {{-- <div class="col-lg-6 latest-update__item">
                        <div class="row">
                            <div class="col-lg-6 latest-update__item--image">
                                <img src="{{asset(''.section($page,'Section 6 Col 1 Image').'')}}">
                            </div>

                            <div class="col-lg-6 latest-update__item--content">
                                <h4><a href="{{url(''.section($page,'Section 6 Col 1 Link').'')}}">{{section($page,'Section 6 Col 1 Header Text')}}</a></h4>
                                <p>{{section($page,'Section 6 Col 1 Header Overview Content')}}</p>

                                <a href="{{url(''.section($page,'Section 6 Col 1 Link').'')}}" class="news-btn"><img src="{{asset('public/images/right-arrow-white.png')}}"></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 latest-update__item">
                        <div class="row">
                            <div class="col-lg-6 latest-update__item--image">
                                <img src="{{asset(''.section($page,'Section 6 Col 2 Image').'')}}">
                            </div>

                            <div class="col-lg-6 latest-update__item--content">
                                <h4><a href="{{url(''.section($page,'Section 6 Col 2 Link').'')}}">{{section($page,'Section 6 Col 2 Header Text')}}</a></h4>
                                <p>{{section($page,'Section 6 Col 2 Header Overview Content')}}</p>

                                <a href="{{url(''.section($page,'Section 6 Col 2 Link').'')}}" class="news-btn"><img src="{{asset('public/images/right-arrow-white.png')}}"></a>
                            </div>
                        </div>
                    </div> --}}

                </div>

                <a href="{{url(''.section($page,'Button Link Section 6').'')}}" class="btn btn--yellow">{{section($page,'Button Text Section 6')}}<img src="{{asset('public/images/right-arrow.png')}}"></a>

                <div class="get-in-touch">
                    <div class="row">
                        <div class="col-lg-6 get-in-touch__content">

                             {!!section($page,'Section 7 Content')!!}

                            {{-- <h3>Get In Touch</h3>
                            <p>Don’t hesitate to contact us to hear more about our performance monitoring and analytics software. We look forward to it.</p> --}}
                        </div>

                        <div class="col-lg-3 get-in-touch__buttons">
                            <a href="{{url(''.section($page,'Button Link Section 7').'')}}" class="btn btn--yellow">{{section($page,'Button Text Section 7')}}</a>
                            <a href="tel:{{section($page,'Button Text Section 7.1')}}" class="btn btn--yellow">{{section($page,'Button Text Section 7.1')}}</a>
                        </div>

                        <div class="col-lg-3 get-in-touch__social-media">
                            <ul>
                                <li><a href="{{settings('SS0013')}}"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="{{settings('SS0015')}}"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>

    @include('front.layouts.sections.footer')
</section>
