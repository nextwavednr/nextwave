@push('extrastylesheets')

<style>

    section.join-team{
        background-image: url("{{ section($page,'Section 7 Image') }}") !important;
    }

</style>
<section class="page page--team">
    @include('front.layouts.sections.header')

    <main class="main-content">

        <section class="innerpage-banner-circle">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6 innerpage-banner-circle__content">

                        {!! section($page,'Content Section 1') !!}

                        {{-- <h1>Our Team</h1>
                        <h2>Experts from the Entire Value-Chain</h2>

                        <p>Decades of experience in Power Quality, Development, Design/ Engineering, Operations & Maintenance (O&M).</p> --}}
                    </div>

                    <div class="col-lg-6 innerpage-banner-circle__image">
                        <img src="{{asset(''. section($page,'Image Section 1') .'')}}">
                    </div>
                </div>
            </div>
        </section>

        <section class="team">
            <div class="wrapper">
                <div class="row">

                    @php
                            
                    $teams = \App\Models\Leadership::where('is_active',1)->get();

                    @endphp

                    @foreach ($teams as $team)
                        
                    <div class="col-lg-4 team__item">
                        <div class="team__item--image">
                            <img src="{{asset(''.$team->banner_image.'')}}">
                        </div>
                        <div class="team__item--details">
                            <h3><a href="{{url('team-details/'.$team->slug.'')}}">{{$team->name}}</a></h3>
                            <h4>{{$team->position}}</h4>
                        </div>
                    </div>

                    @endforeach

                    {{-- <div class="col-lg-4 team__item">
                        <div class="team__item--image">
                            <img src="{{asset('public/images/team01.jpg')}}">
                        </div>
                        <div class="team__item--details">
                            <h3><a href="{{url('team-details')}}">Nader Yarpezeshkan</a></h3>
                            <h4>CEO Founder</h4>
                        </div>
                    </div>

                    <div class="col-lg-4 team__item">
                        <div class="team__item--image">
                            <img src="{{asset('public/images/team02.jpg')}}">
                        </div>

                        <div class="team__item--details">
                            <h3><a href="#">Martin Scotts</a></h3>
                            <h4>General Manager, Supply Chain</h4>
                        </div>
                    </div>

                    <div class="col-lg-4 team__item">
                        <div class="team__item--image">
                            <img src="{{asset('public/images/team03.jpg')}}">
                        </div>

                        <div class="team__item--details">
                            <h3><a href="#">Ellin Landall</a></h3>
                            <h4>Account Manager</h4>
                        </div>
                    </div>

                    <div class="col-lg-4 team__item">
                        <div class="team__item--image">
                            <img src="{{asset('public/images/team04.jpg')}}">
                        </div>

                        <div class="team__item--details">
                            <h3><a href="#">Teshou Kanemoto</a></h3>
                            <h4>CFO, Founder</h4>
                        </div>
                    </div>

                    <div class="col-lg-4 team__item">
                        <div class="team__item--image">
                            <img src="{{asset('public/images/team05.jpg')}}">
                        </div>

                        <div class="team__item--details">
                            <h3><a href="#">Jonny Miranda</a></h3>
                            <h4>Account Manager</h4>
                        </div>
                    </div>

                    <div class="col-lg-4 team__item">
                        <div class="team__item--image">
                            <img src="{{asset('public/images/team06.jpg')}}">
                        </div>

                        <div class="team__item--details">
                            <h3><a href="#">Redda Angeles</a></h3>
                            <h4>Account Manager</h4>
                        </div>
                    </div> --}}

                </div>
            </div>
        </section>  

        <section class="join-team">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6 join-team__empty"></div>

                    <div class="col-lg-6 join-team__details">

                        {!! section($page,'Section 7 Content') !!}

                        {{-- <h3>Join Our Team</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> --}}

                        <a href="{{url(''.section($page,'Button Link Section 7').'')}}" class="btn btn--yellow">{{section($page,'Button Text Section 7')}}</a>
                    </div>
                </div>
            </div>
        </section>
        
    </main>
    @include('front.layouts.sections.footer')
</section>