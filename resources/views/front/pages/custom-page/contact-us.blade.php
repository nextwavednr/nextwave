<section class="page page--contact">
    @include('front.layouts.sections.header')

    <main class="main-content">

        <section class="innerpage-banner-circle">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6 innerpage-banner-circle__content">

                        {!!section($page,'Content Section 1')!!}

                        {{-- <h1>Contact Us</h1>
                        <h2>How may we help you?</h2> --}}
                    </div>

                    <div class="col-lg-6 innerpage-banner-circle__image">
                        <img src="{{asset(''.section($page,'Image Section 1').'')}}">
                        {{-- <img src="{{asset('public/images/homebanner-img.jpg')}}"> --}}
                    </div>
                </div>
            </div>
        </section>

        <section class="contact">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6 contact__details">

                        {!!section($page,'Header-Content Section 2')!!}
                        {{-- <h3>Contact Information</h3>
                        <p>Do you need help from the Next Wave Support Team? Don’t hesitate to contact us. We look forward to hearing from you.</p> --}}

                        <ul>
                            <li>
                                <span class="icon">
                                    <img src="{{asset('public/images/map-icon.png')}}">
                                </span>

                                <span class="contact-label">address:</span>

                                <span class="contact-details">

                                    {!!section($page,'Section 2 Address')!!}

                                    {{-- <p>Next Wave Energy Monitoring, Inc.<br/>
                                    <strong>3142 Tiger Run Court, #118 Carlsbad, CA 92010</strong></p> --}}
                                </span>
                            </li>

                            <li>
                                <span class="icon">
                                    <img src="{{asset('public/images/mobile-icon.png')}}">
                                </span>

                                <span class="contact-label">phone:</span>

                                <span class="contact-details">
                                    {{-- <p>(858) 775-3888 </p> --}}
                                    <p><a href="tel:{{section($page,'Section 2 Phone')}}">{{section($page,'Section 2 Phone')}}</a></p>
                                </span>
                            </li>

                            <li>
                                <span class="icon">
                                    <img src="{{asset('public/images/email-icon.png')}}">
                                </span>

                                <span class="contact-label">email:</span>

                                <span class="contact-details">
                                    {{-- <p>support@nextwavemonitoring.com</p> --}}
                                    <p><a href="mailto:{{section($page,'Section 2 Email')}}">{{section($page,'Section 2 Email')}}</a></p>
                                </span>
                            </li>
                        </ul>
                    </div>

                    <div class="col-lg-6 contact__form">
                        <h3>Send us a message</h3>

                        {{  Form::open([
                            'method' => 'POST',
                            'id' => 'create-contact',
                            'route' => ['contact.store'],
                            'class' => '',
                            ])
                        }}

                            <div class="input-group half">
                                <input id="firstname" type="text" class="form-control " name="firstname" required placeholder="Name">
                            </div>

                            <div class="input-group half">
                                <input id="email" type="text" class="form-control " name="email" required placeholder="Email">
                            </div>

                            <div class="input-group half">
                                <input id="pnumber" type="tel" class="form-control " name="pnumber" required placeholder="Phone Number">
                            </div>

                            <div class="input-group half">
                                <input id="subject" type="text" class="form-control " name="subject" required placeholder="Subject">
                            </div>

                            <div class="input-group">
                                <textarea col="5" id="message" type="text" class="form-control " name="message" required placeholder="Message"></textarea>
                            </div>
                            <div class="input-group justify-content-center">
                                <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                    <div class="col-xs-12 text-center">
                                        {!! NoCaptcha::display() !!}
                                        @if ($errors->has('g-recaptcha-response'))
                                            <span id="g-recaptcha-response-error" class="help-block animation-slideDown">
                                                <p style="color: red">{{ $errors->first('g-recaptcha-response') }}</p>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn--yellow">Submit<img src="{{asset('public/images/right-arrow.png')}}"></button>

                        {{ Form::close() }}
                    </div>
                       
                </div>
            </div>
        </section>

    </main>
    @include('front.layouts.sections.footer')
</section>
@push('extrascripts')
{!! NoCaptcha::renderJs() !!}
<script type="text/javascript" src="{{ asset('public/js/libraries/contacts.js') }}"></script>
@endpush