<section class="page page--careers">
    @include('front.layouts.sections.header')

    <main class="main-content">
       
        <section class="innerpage-banner-circle">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6 innerpage-banner-circle__content">

                        {!!section($page,'Content Section 1')!!}

                        {{-- <h1>Careers</h1>
                        <h2>At Next Wave, we believe it’s the team that makes the difference.</h2>

                        <p>Let’s talk about what you’re passionate about and where you see yourself. Together we can explore the possibilities.</p> --}}

                    </div>

                    <div class="col-lg-6 innerpage-banner-circle__image">
                        <img src="{{asset(''.section($page,'Image Section 1').'')}}">
                    </div>
                </div>
            </div>
        </section>

        <section class="careers">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-8 careers__listing">
                        <h3>{{section($page,'Section 2 Header')}}</h3>

                        <div id="accordion" class="myaccordion">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <div class="mb-0">
                                        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                            <div class="job-title">    
                                                <h4>{{section($page,'Section 2 Item 1 Title')}}</h4>
                                                <!-- <h5>San Diego, CA</h5> -->
                                            </div>
                                            
                                            <span class="fa-stack fa-sm">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-long-arrow-alt-down fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </button>
                                    </div>
                                </div>

                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">

                                        {!!section($page,'Section 2 Item 1 Content')!!}

                                        {{-- <h4>Job Description</h4>

                                        <p>We are seeking Front-End Software Developers/ Engineers for involvement in UI/ UX/ and applications development. If you have interest, please apply so we can talk further.</p>
                                        <ul>
                                            <li>Education in software engineering, computer science or like fields</li>
                                            <li>Ability to write HTML, JavaScript and C++</li>
                                            <li>Familiar with producing and maintaining a dynamic UI</li>
                                            <li>Optimize applications for speed and scalability</li>
                                            <li>Follow QA/ QC protocols and collaborate with team members</li>
                                        </ul> --}}
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <div class="mb-0">
                                        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <div class="job-title">    
                                                <h4>{{section($page,'Section 2 Item 2 Title')}}</h4>
                                                <!-- <h5>San Diego, CA</h5> -->
                                            </div>
                                            
                                            <span class="fa-stack fa-sm">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-long-arrow-alt-down fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </button>
                                    </div>
                                </div>

                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        {!!section($page,'Section 2 Item 2 Content')!!}

                                        {{-- <h4>Job Description</h4>

                                        <p>We are seeking Back-End Software Developers/ Engineers to build server-side application logic, infrastructure software, database design, and future implementations for the software. If we have your interest, please apply so we talk further.</p>
                                        <ul>
                                            <li>Education and involvement in a STEM field </li>
                                            <li>Experience in developing back-end data analytics software, and troubleshooting software and data issues</li>
                                            <li>Familiar with ticketing/ case management systems to document issues or new functionality, diligently track open-items and effectively resolve issues</li>
                                        </ul> --}}
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <div class="mb-0">
                                        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            <div class="job-title">    
                                                <h4>{{section($page,'Section 2 Item 3 Title')}}</h4>
                                                <!-- <h5>San Diego, CA</h5> -->
                                            </div>
                                            
                                            <span class="fa-stack fa-sm">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-long-arrow-alt-down fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </button>
                                    </div>
                                </div>

                                <div id="collapseThree" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">

                                        {!!section($page,'Section 2 Item 3 Content')!!}

                                        {{-- <h4>Job Description</h4>

                                        <p>We are seeking Field Service Engineers for the installation and troubleshooting Field Service Technician for industrial communications. A summary of some qualifications and responsibilities are listed below. If we have your interest, please apply so we talk further.</p>
                                        <ul>
                                            <li>Experience with common industrial communications protocols (i.e., Modbus, RS-485, Ethernet, TCP/ IP and port forwarding, etc.)</li>
                                            <li>Familiar with 3-phase power usage and troubleshooting (277/ 480VAC).</li>
                                            <li>Install and troubleshoot dataloggers and Modbus mapping, and small industrial DC power supplies</li>
                                            <li>Follow common safety practices of industrial electrical field work, safely work at heights, use ladders, lifts, platforms, and to implement fall protection safety practices</li>
                                            <li>Effectively use a multimeter, identify cleared overcurrent protection devices (fuses, breakers, etc.)</li>
                                            <li>Travel</li>
                                        </ul> --}}
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <div class="mb-0">
                                        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            <div class="job-title">    
                                                <h4>{{section($page,'Section 2 Item 4 Title')}}</h4>
                                                <!-- <h5>San Diego, CA</h5> -->
                                            </div>
                                            
                                            <span class="fa-stack fa-sm">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-long-arrow-alt-down fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </button>
                                    </div>
                                </div>

                                <div id="collapseFour" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">

                                        {!!section($page,'Section 2 Item 4 Content')!!}

                                        {{-- <h4>Job Description</h4>

                                        <p>We are seeking Support Specialists to assist our growing customer base with coordinating new installation, technical issues and internal coordination efforts. A summary of some responsibilities and qualifications are listed below. If we have your interest, please apply so we talk further.</p>

                                        <ul>
                                            <li>Experience with customer service roles (i.e., Tier I-III)</li>
                                            <li>Familiar with ticketing/ case management systems to document requests, diligently track open-items and effectively resolve issues</li>
                                            <li>Inbound customer inquiries, support technical issues (hardware and software) in real-time and promote culture of customer loyalty</li>
                                            <li>Identify problems and work with the team to address the concerns</li>
                                            <li>Initiative to run reports on open and closed issues to help document potential trends for process improvement strategies.</li>
                                        </ul> --}}
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <div class="mb-0">
                                        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                            <div class="job-title">    
                                                <h4>{{section($page,'Section 2 Item 5 Title')}}</h4>
                                                <!-- <h5>San Diego, CA</h5> -->
                                            </div>
                                            
                                            <span class="fa-stack fa-sm">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-long-arrow-alt-down fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </button>
                                    </div>
                                </div>

                                <div id="collapseFive" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        {!!section($page,'Section 2 Item 5 Content')!!}

                                        {{-- <h4>Job Description</h4>

                                        <p>We are seeking Customer Success Managers to oversee and manage the process of customer retention, manage our customer support team, develop best practices, and the overarching mission to increase customer loyalty. A summary of some responsibilities and qualifications are listed below. If we have your interest, please apply so we talk further.</p>

                                        <ul>
                                            <li>Experience with customer success roles and managing customer and partner activities</li>
                                            <li>Lead customer retention efforts, mentor and recruit customer support specialists, and promote culture of customer loyalty</li>
                                            <li>Improve internal processes to remedy trends that may potentially impact the customer experience, and KPI tracking and reporting</li>
                                            <li>Involvement with the product roadmap</li>
                                            <li>Maintain regular communication with customers to be closer to downstream experiences with the platform and service</li>
                                            <li>Identify potential topics for news and blog releases which will interest existing and new potential customers</li>
                                        </ul> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 careers__ads">

                    

                        {{-- <h3>Apply Today!</h3> --}}
                        <h3>{{section($page,'Side Header')}}</h3>

                        {{-- <p>If you’re interested in hearing more about open positions at Next Wave, apply by clicking the button below. We look forward to meeting you.</p> --}}
                        {!!section($page,'Side Content')!!}

                        <a href="{{url(''.section($page,'Side Button Link').'')}}" class="btn btn--yellow">{{section($page,'Side Button Text')}}</a>
                    </div>
                </div>
            </div>
        </section>

        <!-- <section class="culture">
            <div class="wrapper">
                <div class="row">
                    <h3>Our Culture</h3>
                </div>          
            </div>

            <div class="culture__slider">
                <div class="culture__slider--item" style="width: 100%; display: inline-block;">
                    <a href="http://44.224.67.159/invisionsd/project-detail/lorem-ipsum-dolor-sit-amet_12" tabindex="-1">
                        <div class="image">
                            <img src="http://44.224.67.159/invisionsd/public/uploads/project_photo/indoor01-1628630872.jpg" alt="">
                        </div>
                    </a>
                </div>

                <div class="culture__slider--item" style="width: 100%; display: inline-block;">
                    <a href="http://44.224.67.159/invisionsd/project-detail/lorem-ipsum-dolor-sit-amet_12" tabindex="-1">
                        <div class="image">
                            <img src="http://44.224.67.159/invisionsd/public/uploads/project_photo/indoor01-1628630872.jpg" alt="">
                        </div>
                    </a>
                </div>

                <div class="culture__slider--item" style="width: 100%; display: inline-block;">
                    <a href="http://44.224.67.159/invisionsd/project-detail/lorem-ipsum-dolor-sit-amet_12" tabindex="-1">
                        <div class="image">
                            <img src="http://44.224.67.159/invisionsd/public/uploads/project_photo/indoor01-1628630872.jpg" alt="">
                        </div>
                    </a>
                </div>

                <div class="culture__slider--item" style="width: 100%; display: inline-block;">
                    <a href="http://44.224.67.159/invisionsd/project-detail/lorem-ipsum-dolor-sit-amet_12" tabindex="-1">
                        <div class="image">
                            <img src="http://44.224.67.159/invisionsd/public/uploads/project_photo/indoor01-1628630872.jpg" alt="">
                        </div>
                    </a>
                </div>

                <div class="culture__slider--item" style="width: 100%; display: inline-block;">
                    <a href="http://44.224.67.159/invisionsd/project-detail/lorem-ipsum-dolor-sit-amet_12" tabindex="-1">
                        <div class="image">
                            <img src="http://44.224.67.159/invisionsd/public/uploads/project_photo/indoor01-1628630872.jpg" alt="">
                        </div>
                    </a>
                </div>
            </div>

            <div class="wrapper">
                <div class="row">
                    <ul>
                        <li >
                            <a href="javascript:void(0)" class="prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <span class="slidercount"></span>
                        </li>
                        <li>
                            <a href="javascript:void(0)" class="next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </section> -->


        

    </main>
    @include('front.layouts.sections.footer')
</section>

<style>
.myaccordion .card,
.myaccordion .card:last-child .card-header {
  border: none;
}

.myaccordion .card-header {
  border-bottom: none;
  background: transparent;
}

.myaccordion .fa-stack {
  font-size: 18px;
}

.myaccordion .btn {
  width: 100%;
  font-weight: bold;
  color: #262626;
  padding: 0;
}

.myaccordion .btn-link:hover,
.myaccordion .btn-link:focus {
  text-decoration: none;
}

.myaccordion li + li {
  margin-top: 10px;
}
</style>