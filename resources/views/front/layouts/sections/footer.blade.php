<footer class="footer">
    <div class="wrapper">
        <div class="row">
            <div class="col-lg-4 footer__logo">
            
            
                {{-- <img src="{{asset('public/images/logo-colored.png')}}" alt="Logo" class="logo"> --}}

                <img src="{{asset(''.settings('SS0010').'')}}" alt="Logo" class="logo">
                
                {{-- <p>Next Wave Energy Monitoring is a Renewable Energy Performance Monitoring & Data Analytics Platform Designed by Power Quality Engineers to Deliver Substantive Energy Vitals to Solar and Energy Storage System End-Users</p> --}}
            
                {!!settings('SS0011')!!}

            </div>

            <div class="col-lg-3 footer__navigation">
                <h4>Main links</h4>
                <ul>
                    <li><a href="{{url('about-us')}}">About Us</a></li>
                    <!-- <li><a href="{{url('team-overview')}}">Our Team</a></li> -->
                    <li><a href="{{url('news-overview')}}">News</a></li>
                    <li><a href="{{url('careers')}}">Careers</a></li>
                    {{-- <li><a href="#">Tutorials</a></li> --}}
                    <li><a href="{{url('contact-us')}}">Contact</a></li>
                </ul>
            </div>

            <div class="col-lg-3 footer__contact-info">
                <h4>Contact Us</h4>

                {{-- <p class="footer__contact-info--location">San Diego Science Center<br/>3030 Bunker Hill Street, Suite 312<br/> San Diego, CA 92109</p> --}}
                <p class="footer__contact-info--location">{!!settings('SS0004')!!}</p>

                {{-- <p class="footer__contact-info--tel"><a href="tel:8582217360">(858) 221-7360</a></p> --}}
                <p class="footer__contact-info--tel"><a href="tel:{{settings('SS0003')}}">{{settings('SS0003')}}</a></p>

                {{-- <p class="footer__contact-info--email"><a href="mailto:info@nwcm.com">info@nwcm.com</a></p> --}}
                <p class="footer__contact-info--email"><a href="mailto:{{settings('SS0002')}}">{{settings('SS0002')}}</a></p>
            
            </div>

            <div class="col-lg-2 footer__social-media">
                <h4>Follow Us</h4>
                <ul>
                    <li><a href="{{settings('SS0013')}}"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="{{settings('SS0015')}}"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<div class="copyright">
    <div class="wrapper">
        <p>{!!settings('SS0007')!!}</p>
    </div>
</div>