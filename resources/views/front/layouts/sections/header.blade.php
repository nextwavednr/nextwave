<header>

    {{-- FOR DESKTOP MENU  --}}
    <div class="main-nagivation-desktop">
        <div class="row">
            <div class="col-lg-3 logo">
                <a href="{{url('/')}}" >
                    <img src="{{asset('public/images/logo.png')}}" alt="Logo" class="logo">
                </a>
            </div>
            <div class="col-6 header-menu">
                <ul>
                    <li>
                        <a href="{{url('about-us')}}">About Us</a>
                        <!-- <div class="dropdown-menu mega-menu">
                            <ul class="sub-menu">
                                <li>
                                    <a class="nav-link" href="#">
                                        <span>About 1</span>
                                    </a>
                                </li>
                                <li>
                                    <a class="nav-link" href="#">
                                        <span>About 2</span>
                                    </a>
                                </li>
                                <li>
                                    <a class="nav-link" href="#">
                                        <span>About 3</span>
                                    </a>
                                </li>
                            </ul>
                        </div> -->
                    </li>
                    <li><a href="{{url('news-overview')}}">News</a></li>
                    <li><a href="{{url('careers')}}">Careers</a></li>
                    {{-- <li><a href="{{url('tutorials')}}">Tutorials</a></li> --}}
                    <li><a href="{{url('contact-us')}}">Contact</a></li>  
                </ul>
            </div>

            <div class="col-lg-3 header-right">
                <span class="user"><a href="#"><i class="far fa-user-circle"></i></a></span>
                <span class="mail"><a href="mailto:{{settings('SS0002')}}"><i class="far fa-envelope"></i></a></span>
                <span class="btn btn--call-btn contact-btn"><a href="tel:{{settings('SS0003')}}"><img src="{{asset('public/images/phone-icon.png')}}" alt="Phone Icon">{{settings('SS0003')}}</a></span>
            </div>
        </div>
    </div>


    {{-- FOR MOBILE MENU  --}}
    <div class="main-navigation-mobile">
        <div class="main-navigation--bottom">
            <div class="wrapper">
                <div class="row align-items-center">
                    <div class="col-6 logo-link">
                        <a href="{{url('/')}}" >
                            <img src="{{asset('public/images/logo.png')}}" alt="Logo" class="logo">
                        </a>
                    </div>
                    <div class="col-6 text-right">
                        <a href="javascript:void(0)" class="hamburger">
                            <div></div>
                        </a>
                        <div class="main-menu">
                            <ul class="accordion">
                                <li><a href="{{url('about-us')}}">About Us</a></li>
                                <li><a href="{{url('news-overview')}}">News</a></li>
                                <li><a href="{{url('careers')}}">Careers</a></li>
                                <li><a href="{{url('contact-us')}}">Contact</a></li>
                            </ul>

                            <ul class="other">
                                <li><span class="user"><a href="#"><i class="far fa-user-circle"></i></a></span></li>  
                                <li><span class="mail"><a href="mailto:{{settings('SS0002')}}"><i class="far fa-envelope"></i></a></span></li>
                                <li><span class="btn btn--call-btn contact-btn"><a href="tel:{{settings('SS0003')}}"><img src="{{asset('public/images/phone-icon.png')}}" alt="Phone Icon">{{settings('SS0003')}}</a></span><li>

                                <!-- <li class="has-dropdown">
                                    <a data-toggle="collapse" class="has-collapse" href="#about-us" >About</a>
                                    <div class="sub-dropdown-menu collapse" id="about-us">
                                        <ul>
                                            <li>
                                                <a class="nav-link" href="{{url('team')}}">
                                                    <span>Team</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="nav-link" href="{{url('about-us')}}">
                                                    <span>About Us</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li> -->
                                <!-- <li><a href="{{url('contact-us')}}">Contact Us</a></li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
