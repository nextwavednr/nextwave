@include('email.header')
<tr>
    <td align="center" bgcolor="#ffffff" style="text-align: left; padding: 20px; color: #555555; font-family: 'Roboto'; font-size: 16px; line-height: 25px;">
        <b>Hi {{ isset($data['user_data']) ? $data['user_data']['name'] : '' }},</b>
        <br>
        <span>Thanks for contacting us at {!! settings('SS0001') !!}.</span>
    </td>
</tr>
<tr>
    <td align="left" bgcolor="#A2D0E7" style="text-align: left; font-weight: 400; padding: 20px; color: #555555; font-family: 'Roboto'; font-size: 16px; line-height: 17px;">
        <h3>Contact Information</h3>
        <p><b>Name: {{ isset($data['user_data']) ? $data['user_data']['name'] : '' }}</b></p>
        <p><b>Email: {{ isset($data['user_data']) && $data['user_data']['email'] ? $data['user_data']['email'] : '' }}</b></p>
        <p><b>Message: {!! isset($data['user_data']) && $data['user_data']['message'] ? $data['user_data']['message'] : '' !!}</b></p>
    </td>
</tr>
@include('email.footer')