<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Career;
use App\Repositories\CareerRepository;

/**
 * Class CareerController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class CareerController extends Controller
{
    /**
     * Career model instance.
     *
     * @var Career
     */
    private $career_model;

    /**
     * CareerRepository repository instance.
     *
     * @var CareerRepository
     */
    private $career_repository;

    /**
     * Create a new controller instance.
     *
     * @param Career $career_model
     * @param CareerRepository $career_repository
     */
    public function __construct(Career $career_model, CareerRepository $career_repository)
    {
        /*
         * Model namespace
         * using $this->career_model can also access $this->career_model->where('id', 1)->get();
         * */
        $this->career_model = $career_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of careers with other data (related tables).
         * */
        $this->career_repository = $career_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        // if (!auth()->user()->hasPermissionTo('Read Career')) {
        //     abort('401', '401');
        // }

        $careers = $this->career_model->get();

        return view('admin.pages.career.index', compact('careers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // if (!auth()->user()->hasPermissionTo('Create Career')) {
        //     abort('401', '401');
        // }

        return view('admin.pages.career.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        // if (!auth()->user()->hasPermissionTo('Create Career')) {
        //     abort('401', '401');
        // }

        $this->validate($request, [
            'name' => 'required|unique:careers,name,NULL,id,deleted_at,NULL',
            'slug' => 'required|unique:careers,slug,NULL,id,deleted_at,NULL',
            'content' => 'required',
            'banner_image' => 'mimes:jpg,jpeg,png',
            'file' => 'mimes:docx,doc,pdf',
        ]);

        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
        $input['slug'] = str_slug($input['name']);

        $career = $this->career_model->create($input);

        if ($request->hasFile('banner_image')) {
            $file_upload_path = $this->career_repository->uploadFile($request->file('banner_image'), /*'banner_image'*/null, 'career_images');
            $career->fill(['banner_image' => $file_upload_path])->save();
        }
        if ($request->hasFile('file')) {
            $file_upload_path = $this->career_repository->uploadFile($request->file('file'), /*'file'*/null, 'career_files');
            $career->fill(['file' => $file_upload_path])->save();
        }

        return redirect()->route('admin.careers.index')->with('flash_message', [
            'title' => '',
            'message' => 'Career ' . $career->name . ' successfully added.',
            'type' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // if (!auth()->user()->hasPermissionTo('Read Career')) {
        //     abort('401', '401');
        // }

        $career = $this->career_model->findOrFail($id);

        return view('admin.pages.career.show', compact('career'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // if (!auth()->user()->hasPermissionTo('Update Career')) {
        //     abort('401', '401');
        // }

        $career = $this->career_model->findOrFail($id);

        return view('admin.pages.career.edit', compact('career'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        // if (!auth()->user()->hasPermissionTo('Update Career')) {
        //     abort('401', '401');
        // }

        $this->validate($request, [
            'name' => 'required|unique:careers,name,' . $id . ',id,deleted_at,NULL',
            'slug' => 'required|unique:careers,slug,' . $id . ',id,deleted_at,NULL',
            'content' => 'required',
            'banner_image' => 'required_if:remove_banner_image,==,1|mimes:jpg,jpeg,png',
            'file' => 'required_if:remove_file,==,1|mimes:docx,doc,pdf',
        ]);

        $career = $this->career_model->findOrFail($id);
        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
        $input['slug'] = str_slug($input['name']);

        if ($request->hasFile('banner_image')) {
            $file_upload_path = $this->career_repository->uploadFile($request->file('banner_image'), /*'banner_image'*/null, 'career_images');
            $input['banner_image'] = $file_upload_path;
        }
        if ($request->has('remove_banner_image') && $request->get('remove_banner_image')) {
            $input['banner_image'] = '';
        }

        if ($request->hasFile('file')) {
            $file_upload_path = $this->career_repository->uploadFile($request->file('file'), /*'file'*/null, 'career_files');
            $input['file'] = $file_upload_path;
        }
        if ($request->has('remove_file') && $request->get('remove_file')) {
            $input['file'] = '';
        }

        $career->fill($input)->save();

        return redirect()->route('admin.careers.index')->with('flash_message', [
            'title' => '',
            'message' => 'Career ' . $career->name . ' successfully updated.',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // if (!auth()->user()->hasPermissionTo('Delete Career')) {
        //     abort('401', '401');
        // }

        $career = $this->career_model->findOrFail($id);
        $career->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'Career successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return response()->json($response);
    }
}
