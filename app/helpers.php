<?php


/**
 * @param null $title
 * @param null $message
 * @return \Illuminate\Foundation\Application|mixed
 * For the flash messages.
 */
function custom_flash($title = null, $message = null) {
    // Set variable $flash to fetch the Flash Class
    // in Flash.php
    $flash = app('App\Http\Flash');

    // If 0 parameters are passed in ($title, $message)
    // then just return the flash instance.
    if (func_num_args() == 0) {
        return $flash;
    }

    // Just return a regular flash->info message
    return $flash->info($title, $message);
}

/**
 * For highlighting of words that matched the keywords.
 *
 * @param null $text
 * @param null $words
 *
 * @return \Illuminate\Foundation\Application|mixed
 */
function highlight_word($text = null, $words = null)
{
    return preg_replace("/\w*?" . preg_quote($text) . "\w*/i", "<b><i>$0</i></b>", $words);
}

/**
 * For highlighting of keywords only.
 *
 * @param null $text
 * @param null $words
 *
 * @return \Illuminate\Foundation\Application|mixed
 */
function highlight_keyword($text = null, $words = null)
{
    $replace = '<b><i>' . $text . '</i></b>';
    $words = str_ireplace($text, $replace, $words);
    return $words;
}

/**
 * @param null string $url
 *
 * @return \Illuminate\Foundation\Application|mixed
 * Add http to url
 */
function add_http($url = null)
{
    if ($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
    }
    return $url;
}

//Global Functions
function CleanUrl($string) {
    $string = strtolower($string);
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    $string = preg_replace("/[\s-]+/", " ", $string);
    $string = preg_replace("/[\s_]/", "-", $string);
    return $string;
}

function section($page,$parameters){
    $repository = new \App\Repositories\SectionRepository();

    return $repository->find($page,$parameters);
}

function getAttachment($id){
    $image = \App\Models\Attachment::find($id);
    return $image;
}

function addSection($name, $type, $pages, $value = '', $pos) {
    // if (empty($value) && $type === \App\Models\Section::FORM)
    //     $value = '{"options": {}, "fields": [], "data": []}';

    // try 
    // {
        
        $checkifexist = \App\Models\Section::leftjoin('page_section','page_section.section_id','=','sections.id')->where('sections.name',$name)->where('page_section.page_id',$pages)->get();

        if(!count($checkifexist))
        {
            $section = \App\Models\Section::updateOrCreate(compact('name', 'type', 'value'));
            $section->pages()->sync($pages);
            \App\Models\PageSection::where('section_id',$section->id)->update(['position' => $pos]);
        }
        else 
        {
            // dd('exist');            
        }

       
    
        // return $section;
    // }
    // catch(\Exception $exception)
    // {

    // }

}

function settings($code = null, $value = null) {
    if (empty($code))
        return \App\Models\SystemSetting::get();

    if (is_array($code))
        return \App\Models\SystemSetting::whereIn('code', $code)->select('code', 'value')->get()->reduce(function ($settings, \App\Models\SystemSetting $setting) {
            $settings[$setting->code] = $setting->value;

            return $settings;
        }, []);

        try{
            return \App\Models\SystemSetting::code($code, $value);
        }
        catch(\Exception $exception)
        {
            return 'err';
        }
}