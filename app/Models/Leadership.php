<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Leadership
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class Leadership extends Model
{
    use SoftDeletes;

    protected $table = 'leaderships';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'position',
        'banner_image',
        'bio',
        'file',
        'info',
        'is_active',
    ];
}