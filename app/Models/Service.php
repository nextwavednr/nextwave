<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Service
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class Service extends Model
{
    use SoftDeletes;

    protected $table = 'services';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'banner_image',
        'file',
        'content',
        'content_1',
        'is_active',
        'seo_meta_id'
    ];

    public final function seoMeta()
    {
        return $this->belongsTo(SeoMeta::class);
    }
}