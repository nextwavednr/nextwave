<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Career
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class Career extends Model
{
    use SoftDeletes;

    protected $table = 'careers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'banner_image',
        'file',
        'content',
        'is_active',
    ];
}