<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SystemSetting
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class SystemSetting extends Model
{
    use SoftDeletes;

    protected $table = 'system_settings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'value',
    ];

    public function scopeCode(Builder $query, $code, $value = null)
    {
        if (empty($value)) {
            $settings = $query->whereCode($code)->first();

            return ($value === 0) ? $settings : $settings->value;
        }

        return $this->newQuery()->whereCode($code)->update(compact('value'));
    }
}